import torch
from models.npgnn.npr_gnn_model import NPRGNNModel
from models.gin_model import GINModel
from models.gnn.acr_gnn import ACRGNNModel
from training.model_trainer import ModelTrainer


ft_grandparent_par = {

    "task_name": r"FT_grandparent",
    "task_type": "binary",
    "output_type": "edge",
    "binary_pred_index": 1,

    "epochs": 200,
    "batch_size": 32,
    "learning_rate": 0.01,
    "weight_decay": 5e-4,  # 为了防止过拟合
    "device": 'cpu',

    # "criterion": torch.nn.BCEWithLogitsLoss,  # Sigmoid+BCELoss
    "criterion": torch.nn.BCELoss,  # 二元交叉熵，针对二分类

    "training_path": r"../datasets/family_tree/train_grandparents.plk",
    "valid_path": r"../datasets/family_tree/valid_grandparents.plk",

    "node_in_channels": 1,
    "node_out_channels": 1,

    "edge_in_channels": 2,
    "edge_out_channels": 1,

    "log_dir": r"log/ft",

}


ft_parent_par = {

    "task_name": r"FT_parent",
    "task_type": "binary",
    "output_type": "edge",
    "binary_pred_index": 2,

    "epochs": 200,
    "batch_size": 32,
    "learning_rate": 0.01,
    "weight_decay": 5e-4,
    "device": 'cpu',

    # "criterion": torch.nn.BCEWithLogitsLoss,  # Sigmoid+BCELoss
    "criterion": torch.nn.BCELoss,   # 二元交叉熵，针对二分类

    "training_path": "../datasets/family_tree/train_parent.plk",
    "valid_path": "../datasets/family_tree/valid_parent.plk",

    "node_in_channels": 1,
    "node_out_channels": 1,

    "edge_in_channels": 3,
    "edge_out_channels": 1,

    "log_dir": r"log/ft"
}


ft_sister_par = {

    "task_name": r"FT_sister",
    "task_type": "binary",
    "output_type": "edge",
    "binary_pred_index": 3,
    "device": "cpu",

    "epochs": 200,
    "batch_size": 32,
    "learning_rate": 0.01,
    "weight_decay": 5e-4,

    # "criterion": torch.nn.BCEWithLogitsLoss,  # Sigmoid+BCELoss
    "criterion": torch.nn.BCELoss,   # 二元交叉熵，针对二分类

    "training_path": r"../datasets/family_tree/train_sister.plk",
    "valid_path": r"../datasets/family_tree/valid_sister.plk",

    "node_in_channels": 1,
    "node_out_channels": 1,

    "edge_in_channels": 4,
    "edge_out_channels": 1,

    "log_dir": r"log/ft"
}


ft_uncle_par = {

    "task_name": r"FT_uncle",
    "task_type": "binary",
    "output_type": "edge",
    "binary_pred_index": 3,


    "epochs": 200,
    "batch_size": 32,
    "learning_rate": 0.01,
    "weight_decay": 5e-4,
    "device": "cpu",

    # "criterion": torch.nn.BCEWithLogitsLoss,  # Sigmoid+BCELoss
    "criterion": torch.nn.BCELoss,   # 二元交叉熵，针对二分类

    "training_path": r"../datasets/family_tree/train_uncle.plk",
    "valid_path": r"../datasets/family_tree/valid_uncle.plk",

    "node_in_channels": 1,
    "node_out_channels": 1,

    "edge_in_channels": 4,
    "edge_out_channels": 1,

    "log_dir": r"log/ft"
}


def train_mpgnn(par):
    model = NPRGNNModel(
        par["node_in_channels"],
        par["node_out_channels"],
        par["edge_in_channels"],
        par["edge_out_channels"],
    )
    trainer = ModelTrainer()
    trainer.initialize(model, par)
    trainer.train_epochs()
    trainer.dump_logs()


def train_gin(par):
    model = GINModel(
        par["edge_in_channels"],
        par["edge_out_channels"],
    )
    trainer = ModelTrainer()
    trainer.initialize(model, par)
    trainer.train_epochs()
    trainer.dump_logs()


def train_acrgnn(par):
    model = ACRGNNModel(
        par["edge_in_channels"],
        par["edge_out_channels"],
    )
    trainer = ModelTrainer()
    trainer.initialize(model, par)
    trainer.train_epochs()
    trainer.dump_logs()


if __name__ == '__main__':
    # train_mpgnn(ft_uncle_par)
    # train_gin(ft_uncle_par)
    train_acrgnn(ft_uncle_par)

