# encoding=utf-8
import torch
from torch_geometric.loader import DataLoader
from zzqlib.io import pickle_util, file_helper


class ModelTrainer:

    def __init__(self):

        self.task_name = ""
        self.log_dir = ""

        # the elements in the list are objects of torch_geometric.data.Data
        # with attributes: x, y, edge_index, edge_attr, edge_labels, ho_index, ho_num
        self.train_loader = None
        self.valid_loader = None
        self.device = "cpu"
        self.batch_size = 32
        self.task_type = "binary"  # binary/multiclass
        self.output_type = "node"
        self.learning_rate = 0
        self.weight_decay = 0
        self.binary_pred_index = -1

        self.model = None
        self.optimizer = None
        self.criterion = None

        # self.criterion_boolean = torch.nn.BCELoss,  # 二元交叉熵，针对二分类
        # self.criterion_multiclass = torch.nn.CrossEntropyLoss      # 针对多分类

        self.logs = []
        self.n_epochs = 0

    def initialize(self, model, parameters):
        par = parameters
        self.task_name = par["task_name"]
        self.log_dir = par["log_dir"]
        self.batch_size = par["batch_size"]

        if "training_path" in parameters:
            train_dataset = pickle_util.load_object(parameters["training_path"])
            valid_dataset = pickle_util.load_object(parameters["valid_path"])
            self.train_loader = DataLoader(train_dataset, batch_size=self.batch_size, shuffle=True)  # pygeo的dataloader会把所有的图组成一个大的，用稀疏矩阵
            self.valid_loader = DataLoader(valid_dataset, batch_size=self.batch_size, shuffle=True)

        elif "training_data" in parameters:
            train_dataset = parameters["training_data"]
            valid_dataset = parameters["valid_data"]
            self.train_loader = DataLoader(train_dataset, batch_size=self.batch_size, shuffle=True)
            self.valid_loader = DataLoader(valid_dataset, batch_size=self.batch_size, shuffle=True)

        self.device = torch.device(par["device"])
        self.output_type = par["output_type"]
        self.task_type = par["task_type"]
        self.learning_rate = par["learning_rate"]
        self.weight_decay = par["weight_decay"]
        self.n_epochs = par["epochs"]
        self.binary_pred_index = par["binary_pred_index"]

        self.model = model
        self.optimizer = torch.optim.Adam(model.parameters(), lr=par["learning_rate"], weight_decay=par["weight_decay"])
        self.criterion = par["criterion"]()

    def train_single_epoch(self, epoch):

        self.model.train()
        train_loss = []
        train_accs = []

        for batch in self.train_loader:

            batch.to(self.device)
            edge_index = batch.edge_index
            ho_index = batch.ho_index
            edge_num = batch.num_edges
            node_num = batch.num_nodes

            # node feature and labels
            n_x = batch.x
            n_y = batch.y

            # edge feature and labels
            e_x = batch.edge_attr
            e_y = batch.edge_labels

            # run model
            n_y_out, e_y_out = self.model(n_x, e_x, edge_index, ho_index, node_num, edge_num, batch.batch)

            if self.output_type == "node":
                y_out = n_y_out
                y_labels = n_y
                if self.task_type == "binary":
                    y_labels = y_labels.select(dim=1, index=self.binary_pred_index).unsqueeze(1)
            else:
                y_out = e_y_out
                y_labels = e_y
                if self.task_type == "binary":
                    y_labels = y_labels.select(dim=1, index=self.binary_pred_index).unsqueeze(1)

            # e_y不需要to device，因为dataset已经to devise了
            loss = self.criterion(y_out, y_labels)
            self.optimizer.zero_grad()
            loss.backward()  # 反向传播
            self.optimizer.step()  # 更新参数

            if self.task_type == "binary":
                preds = torch.round(y_out)
                correct = torch.eq(preds, y_labels).float()
                correct_count = correct.sum()
                correct_num = len(correct)
            else:
                preds = y_out.ge(0.5)
                correct_num = len(preds)*y_labels.shape[1]
                correct_count = torch.eq(preds, y_labels).float().sum()

            acc = correct_count / correct_num
            train_loss.append(loss.item())
            train_accs.append(acc.item())

        current_loss = sum(train_loss) / len(train_loss)
        current_acc = sum(train_accs) / len(train_accs)

        print(f"[ Train | {epoch + 1:03d}/{self.n_epochs:03d} ] loss = {current_loss:.5f}, acc = {current_acc:.5f}")
        self.logs.append(f"[ Train | {epoch + 1:03d}/{self.n_epochs:03d} ] loss = {current_loss:.5f}, acc = {current_acc:.5f}")

    def valid_single_epoch(self, epoch):

        self.model.eval()
        valid_loss = []
        valid_accs = []

        for batch in self.valid_loader:

            batch.to(self.device)
            edge_index = batch.edge_index
            ho_index = batch.ho_index
            edge_num = batch.num_edges
            node_num = batch.num_nodes

            # node feature and labels
            n_x = batch.x
            n_y = batch.y

            # edge feature and labels
            e_x = batch.edge_attr
            e_y = batch.edge_labels

            # run model
            with torch.no_grad():
                n_y_out, e_y_out = self.model(n_x, e_x, edge_index, ho_index, node_num, edge_num, batch.batch)

            if self.output_type == "node":
                y_out = n_y_out
                y_labels = n_y
                if self.task_type == "binary":
                    y_labels = y_labels.select(dim=1, index=self.binary_pred_index).unsqueeze(1)
            else:
                y_out = e_y_out
                y_labels = e_y
                if self.task_type == "binary":
                    y_labels = y_labels.select(dim=1, index=self.binary_pred_index).unsqueeze(1)

            # e_y不需要to device，因为dataset已经to devise了
            loss = self.criterion(y_out, y_labels)

            if self.task_type == "binary":
                preds = torch.round(y_out)
                correct = torch.eq(preds, y_labels).float()
                correct_count = correct.sum()
                correct_num = len(correct)
            else:
                preds = y_out.ge(0.5)
                correct_num = len(preds)*y_labels.shape[1]
                correct_count = torch.eq(preds, y_labels).float().sum()

            acc = correct_count / correct_num
            valid_loss.append(loss.item())
            valid_accs.append(acc.item())

        current_loss = sum(valid_loss) / len(valid_loss)
        current_acc = sum(valid_accs) / len(valid_accs)

        print(f"[ Valid | {epoch + 1:03d}/{self.n_epochs:03d} ] loss = {current_loss:.5f}, acc = {current_acc:.5f}")
        self.logs.append(f"[ Valid | {epoch + 1:03d}/{self.n_epochs:03d} ] loss = {current_loss:.5f}, acc = {current_acc:.5f}")

    def train_epochs(self):
        print(f"Training {self.model.__class__.__name__} on {self.task_name} [{self.task_type}]")

        for epoch in range(self.n_epochs):
            self.train_single_epoch(epoch)
            self.valid_single_epoch(epoch)

    def dump_logs(self):
        model_name = self.model.__class__.__name__
        log_path = "%s/%s_%s.txt" % (self.log_dir, self.task_name, model_name)
        file_helper.write_file_as_unicode(log_path, self.logs)
