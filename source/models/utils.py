import torch


def compute_ho_index(edge_index, considered_relations=["selfish", "precursor", "succeed", "same-out", "same-in"]):
    """
    compute high-order relations between edges of four types:
    for edge (x,y)

    selfish: (x,y)
    precursor: (z,x)
    succeed: (y,z)
    same-out: (x,z)
    same-in: (z,y)

    :return: ho_index
    """
    edge_index_permuted = edge_index.permute([1, 0])

    edge_dict = {}
    for edge_id, node_pair in enumerate(edge_index_permuted):
        edge_dict[edge_id] = {}
        edge_dict[edge_id]["node_pair"] = node_pair
        edge_dict[edge_id]["precursor"] = []
        edge_dict[edge_id]["succeed"] = []
        edge_dict[edge_id]["same-out"] = []
        edge_dict[edge_id]["same-in"] = []
        edge_dict[edge_id]["selfish"] = []

    for edge_i in edge_dict:

        # selfish
        edge_dict[edge_i]["selfish"].append(edge_i)

        for edge_j in edge_dict:
            if edge_i != edge_j:
                node_pair_i = edge_dict[edge_i]["node_pair"]
                node_pair_j = edge_dict[edge_j]["node_pair"]

                if node_pair_i[0] == node_pair_j[1]:
                    # precursor: edge_i(x,y), edge_j(z,x)
                    edge_dict[edge_i]["precursor"].append(edge_j)

                if node_pair_i[1] == node_pair_j[0]:
                    # succeed: edge_i(x,y), edge_j(y,z)
                    edge_dict[edge_i]["succeed"].append(edge_j)

                if node_pair_i[0] == node_pair_j[0]:
                    # same-out: edge_i(x,y), edge_j(x,z)
                    edge_dict[edge_i]["same-out"].append(edge_j)

                if node_pair_i[1] == node_pair_j[1]:
                    # same-in: edge_i(x,y), edge_j(z,y)
                    edge_dict[edge_i]["same-in"].append(edge_j)

    # print(edge_dict)

    consider_relations = considered_relations
    bridge_start = []
    bridge_end = []
    for edge_i in edge_dict:
        all_neighbors = []
        for rel in consider_relations:
            all_neighbors.extend(edge_dict[edge_i][rel])
        all_neighbors = list(set(all_neighbors))
        for neighbor in all_neighbors:
            bridge_start.append(edge_i)
            bridge_end.append(neighbor)

    ho_index = torch.tensor([bridge_start, bridge_end], dtype=torch.long)
    # print(bridge_index)
    return ho_index


def compute_edge_batch(batch, edge_index):
    """

    :param batch: tensor(node_num,) int64
    :param edge_index: tensor(2, edge_num) long
    :return: edge_batch: tensor(edge_num,) int64
    """
    edge_batch = torch.zeros([edge_index.shape[1]], dtype=torch.int64)
    for i in range(edge_index.shape[1]):
        edge_batch[i] = batch[edge_index[0][i].item()]
    return edge_batch


if __name__ == '__main__':
    _batch = torch.tensor([0, 0, 0, 0, 1, 1])
    _edge_index = torch.tensor([[0, 1, 1, 2, 3, 4], [1, 0, 2, 0, 0, 5]], dtype=torch.long)
    # _ho_index = compute_ho_index(_edge_index)
    # print(_ho_index)
    compute_edge_batch(_batch, _edge_index)

