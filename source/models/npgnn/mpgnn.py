import torch
from torch_geometric.nn.conv.message_passing import MessagePassing
from torch_scatter import scatter
from torch.nn import Linear, Parameter


class BooleanNPRGNN(MessagePassing):
    r"""
    .. math::
        \mathbf{e}_i^{(t+1)}=\mathbf{H}\mathbf{e}_i^{(t)}+(\sum_{\mathbf{e}_k\in\mathcal{HO}(\mathbf{e}_i)}{\mathbf{W}_k\mathbf{e}_k^{(t)}}+\mathbf{b})+\mathbf{d}

    """

    def __init__(self,
                 in_channels: int,
                 hidden_channels: int,
                 edge_attr='sum'):
        super().__init__()

        self.edge_aggr = edge_attr

        # hidden layer
        self.linear_neighbor = Linear(in_channels, hidden_channels, bias=False)
        self.bias_neighbor = Parameter(torch.Tensor(hidden_channels))
        self.linear_self = Linear(in_channels, hidden_channels, bias=False)
        self.bias_self = Parameter(torch.Tensor(hidden_channels))

        # output layer
        self.linear_out = Linear(hidden_channels, 1)
        self.relu = torch.nn.ReLU()
        self.sigmoid = torch.nn.Sigmoid()

        self.reset_parameters()

    def reset_parameters(self):
        self.linear_neighbor.reset_parameters()
        self.bias_neighbor.data.zero_()
        self.linear_self.reset_parameters()
        self.bias_self.data.zero_()
        self.linear_out.reset_parameters()

    def forward(self, e, edge_index, ho_index, edge_num, batch=None):
        """

        :param e: [num, dim_num]
        :param edge_index:
        :param ho_index:
        :param edge_num:
        :param batch:
        :return: out [num,]
        """
        # self.propagate(edge_index, x=x)  # this is for node updating

        # hidden layer
        adj_e = self.edge_updater(edge_index, ho_index=ho_index, edge_num=edge_num, e=e)
        adj_e = self.linear_neighbor(adj_e) + self.bias_neighbor
        e = self.linear_self(e) + self.bias_self
        new_e = adj_e + e
        # new_e = self.relu(new_e)

        # output layer
        out = self.sigmoid(self.linear_out(new_e))
        return out

    def edge_update(self, edge_index, ho_index, edge_num, e):
        # edge_index is useless

        # select
        source_index = ho_index[1]
        target_index = ho_index[0]
        # collect all high-order neighborhoods
        collect_ = e.index_select(dim=0, index=source_index)

        # aggregate
        aggr_e = scatter(collect_, target_index, dim=0, dim_size=edge_num, reduce=self.edge_aggr)

        return aggr_e
