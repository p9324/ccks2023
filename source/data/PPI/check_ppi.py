# encoding=utf-8
import torch
import torch_geometric.transforms as T
from torch_geometric.datasets import PPI
from torch_geometric.loader import DataLoader


def check_ppi():
    #pre_transform = T.Compose([T.GCNNorm(), T.ToSparseTensor()])
    root = r"../../datasets/PPI/raw"
    train_dataset = PPI(root, split='train')
    val_dataset = PPI(root, split='val')
    test_dataset = PPI(root, split='test')
    for dataset in [train_dataset, val_dataset, test_dataset]:
        dataset.data.ho_index = torch.Tensor([])
        dataset.data.edge_labels = torch.Tensor([])
    train_loader = DataLoader(train_dataset, batch_size=2, shuffle=True)
    val_loader = DataLoader(val_dataset, batch_size=2, shuffle=False)
    test_loader = DataLoader(test_dataset, batch_size=2, shuffle=False)

    print("data")


if __name__ == '__main__':
    check_ppi()
