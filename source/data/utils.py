# encoding=utf-8

class C2GraphData:

    def __init__(self):

        self.nodes = {}  # key: node name, value: node id
        self.node_labels = {}  # key: node label, value: label id
        self.nid_to_lid = {}  # key: node id, value: list of label id
        self.num_of_nodes = -1
        self.num_of_node_labels = -1
        self.nid_generator = 0
        self.nlabel_generator = 0

        self.edges = {}      # key: edge id, value: [nid_out, nid_in]
        self.edges_out = {}  # supper key: nid_out, sub key: nid_in, value: edge id
        self.edges_in = {}   # supper key: nid_in, sub key: nid_out, value: edge id

        self.edge_labels = {}  # key: edge label, value: label id
        self.eid_to_lid = {}  # key: edge id, value: list of label id
        self.num_of_edges = -1
        self.num_of_edge_labels = -1
        self.eid_generator = 0
        self.elabel_generator = 0

    # ----------------------------
    # GET PROPERTIES
    # ----------------------------

    def node_num(self):
        return len(self.nodes.keys())

    def node_label_num(self):
        return len(self.node_labels.keys())

    def nodes_outgoing(self):
        return list(self.edges_out.keys())

    def get_node_label_by_nid(self, nid, single=True):
        if single:
            return self.nid_to_lid[nid][0]
        else:
            return self.nid_to_lid[nid]

    def get_node_ids(self):
        return list(self.nid_to_lid.keys())

    def edge_num(self):
        return len(self.edges.keys())

    def edge_label_num(self):
        return len(self.edge_labels.keys())

    def get_edge_by_id(self, edge_id):
        return self.edges[edge_id]

    # def get_edge_id(self, nid_out, nid_in):
    #     return self.edges_out[nid_out][nid_in]

    # ----------------------------
    # ADD NODE
    # ----------------------------

    def add_node(self, node_name):
        if node_name in self.nodes:
            node_id = self.nodes[node_name]
        else:
            node_id = self.__generate_node_id(node_name)
            self.nodes[node_name] = node_id
        return node_id

    def add_node_label(self, node_label):
        if node_label in self.node_labels:
            nlabel_id = self.node_labels[node_label]
        else:
            nlabel_id = self.__generate_node_label_id(node_label)
            self.node_labels[node_label] = nlabel_id
        return nlabel_id

    def add_node_with_labels(self, node_name, node_labels):
        nid = self.add_node(node_name)

        for node_label in node_labels:
            lid = self.add_node_label(node_label)
            if nid not in self.nid_to_lid:
                self.nid_to_lid[nid] = []

            if lid not in self.nid_to_lid[nid]:
                self.nid_to_lid[nid].append(lid)

        return self.nodes[node_name]

    def __generate_node_id(self, node_name):
        node_id = None
        if node_name not in self.nodes:
            node_id = self.nid_generator
            self.nid_generator += 1
        else:
            node_id = self.nodes[node_name]
        return node_id

    def __generate_node_label_id(self, node_label):
        nlabel_id = None
        if node_label not in self.node_labels:
            nlabel_id = self.nlabel_generator
            self.nlabel_generator += 1
        else:
            nlabel_id = self.node_labels[node_label]
        return nlabel_id

    # ----------------------------
    # ADD EDGES
    # ----------------------------

    def add_edge(self, node_name_out, node_name_in):
        nid_out = self.add_node(node_name_out)
        nid_in = self.add_node(node_name_in)
        edge_id = self.add_edge_by_nids(nid_out, nid_in)

        if edge_id not in self.edges:
            self.edges[edge_id] = (nid_out, nid_in)

        return edge_id

    def add_edge_label(self, edge_label):

        if edge_label not in self.edge_labels:
            elabel_id = self.__generate_edge_label_id(edge_label)
            self.edge_labels[edge_label] = elabel_id
        else:
            elabel_id = self.edge_labels[edge_label]
        return elabel_id

    def add_edge_with_labels(self, node_name_out, node_name_in, edge_labels):
        edge_id = self.add_edge(node_name_out, node_name_in)
        for edge_label in edge_labels:
            elabel_id = self.add_edge_label(edge_label)
            if edge_id not in self.eid_to_lid:
                self.eid_to_lid[edge_id] = []

            if elabel_id not in self.eid_to_lid[edge_id]:
                self.eid_to_lid[edge_id].append(elabel_id)

        return edge_id

    def add_edge_by_nids(self, nid_out, nid_in):
        edge_id1 = self.__add_edge_from_out_to_in(nid_out, nid_in)
        edge_id2 = self.__add_edge_from_in_to_out(nid_in, nid_out)
        if edge_id2 != edge_id1:
            print("ERROR:[add_edge_by_nids]edge_id inconsistent")
        return edge_id1

    def __add_edge_from_out_to_in(self, nid_out, nid_in):
        edge_id = self.__generate_edge_id(nid_out, nid_in)

        if nid_out in self.edges_out:
            nid_ins = self.edges_out[nid_out]
            if nid_in in nid_ins:
                if self.edges_out[nid_out][nid_in] != edge_id:
                    print("Error:[__add_edge_from_out_to_in] edge id inconsistent")
            else:
                self.edges_out[nid_out][nid_in] = edge_id
        else:
            self.edges_out[nid_out] = {}
            self.edges_out[nid_out][nid_in] = edge_id

        return edge_id

    def __add_edge_from_in_to_out(self, nid_in, nid_out):
        edge_id = self.__generate_edge_id(nid_out, nid_in)

        if nid_in in self.edges_in:
            nid_outs = self.edges_in[nid_in]
            if nid_out in nid_outs:
                if self.edges_in[nid_in][nid_out] != edge_id:
                    print("Error:[__add_edge_from_in_to_out] edge id inconsistent")
            else:
                self.edges_in[nid_in][nid_out] = edge_id
        else:
            self.edges_in[nid_in] = {}
            self.edges_in[nid_in][nid_out] = edge_id

        return edge_id

    def __generate_edge_id(self, nid_out, nid_in):
        edge_id = None
        if nid_out in self.edges_out:
            nid_ins = self.edges_out[nid_out]
            if nid_in in nid_ins:
                edge_id = nid_ins[nid_in]
            else:
                edge_id = self.eid_generator
                self.eid_generator += 1
        else:
            edge_id = self.eid_generator
            self.eid_generator += 1

        return edge_id

    def __generate_edge_label_id(self, edge_label):
        if edge_label not in self.edge_labels:
            elabel_id = self.elabel_generator
            self.elabel_generator += 1
        else:
            elabel_id = self.edge_labels[edge_label]
        return elabel_id

    def get_edge_id(self, nid_out, nid_in):
        if nid_out in self.edges_out:
            nid_ins = self.edges_out[nid_out]
            if nid_in in nid_ins:
                return nid_ins[nid_in]
        return None

    def get_edge_label_by_id(self, eid, single=True):
        if single:
            return self.eid_to_lid[eid][0]
        else:
            return self.eid_to_lid[eid]

    # ----------------------------------
    # HIGH-ORDER EDGES
    # ----------------------------------

    def compute_ho_index(self, considered_relations=["selfish", "precursor", "succeed", "same-out", "same-in"]):
        """
        compute high-order relations between edges of four types:
        for edge (x,y)

        selfish: (x,y)
        precursor: (z,x)
        succeed: (y,z)
        same-out: (x,z)
        same-in: (z,y)

        :return: ho_index
        """

        edge_dict = {}
        for edge_id in self.edges:
            node_pair = self.edges[edge_id]
            edge_dict[edge_id] = {}
            edge_dict[edge_id]["node_pair"] = node_pair
            edge_dict[edge_id]["precursor"] = []
            edge_dict[edge_id]["succeed"] = []
            edge_dict[edge_id]["same-out"] = []
            edge_dict[edge_id]["same-in"] = []
            edge_dict[edge_id]["selfish"] = []

        for edge_i in edge_dict:

            (x, y) = self.edges[edge_i]

            # selfish
            edge_dict[edge_i]["selfish"].append(edge_i)

            # precursor: edge_i(x,y), edge_j(z,x)
            if x in self.edges_in:
                precursors = self.edges_in[x]
                for z in precursors:
                    edge_j = self.get_edge_id(z, x)
                    if edge_j not in edge_dict[edge_i]["precursor"]:
                        edge_dict[edge_i]["precursor"].append(edge_j)

            # succeed: edge_i(x,y), edge_j(y,z)
            if y in self.edges_out:
                succeeds = self.edges_out[y]
                for z in succeeds:
                    edge_j = self.get_edge_id(y, z)
                    if edge_j not in edge_dict[edge_i]["succeed"]:
                        edge_dict[edge_i]["succeed"].append(edge_j)

            # same-out: edge_i(x,y), edge_j(x,z)
            if x in self.edges_out:
                sameouts = self.edges_out[x]
                for z in sameouts:
                    edge_j = self.get_edge_id(x, z)
                    if edge_j not in edge_dict[edge_i]["same-out"]:
                        edge_dict[edge_i]["same-out"].append(edge_j)

            # same-in: edge_i(x,y), edge_j(z,y)
            if y in self.edges_in:
                sameins = self.edges_in[y]
                for z in sameins:
                    edge_j = self.get_edge_id(z, y)
                    if edge_j not in edge_dict[edge_i]["same-in"]:
                        edge_dict[edge_i]["same-in"].append(edge_j)

        # filter

        consider_relations = considered_relations
        bridge_start = []
        bridge_end = []
        for edge_i in edge_dict:
            all_neighbors = []
            for rel in consider_relations:
                all_neighbors.extend(edge_dict[edge_i][rel])
            all_neighbors = list(set(all_neighbors))
            for neighbor in all_neighbors:
                bridge_start.append(edge_i)
                bridge_end.append(neighbor)

        return [bridge_start, bridge_end]
