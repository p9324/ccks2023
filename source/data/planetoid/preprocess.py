from zzqlib.io import pickle_util, csv_util
from zzqlib.common import process_bar


class CitationData:

    def __init__(self):
        self.papers = {}
        self.paper_citation = {}

    def add_paper(self, paper_id, paper_type):
        if paper_id not in self.papers:
            self.papers[paper_id] = []

        self.papers[paper_id].append(paper_type)

    def add_citation(self, cited_id, citation_id):

        if cited_id not in self.papers:
            print(f"{cited_id} is not in the list of papers.")

        if citation_id not in self.papers:
            print(f"{citation_id} is not in the list of papers.")

        if cited_id not in self.paper_citation:
            self.paper_citation[cited_id] = []

        self.paper_citation[cited_id].append(citation_id)

    def num_of_citations(self):
        return sum([len(self.paper_citation[i]) for i in self.paper_citation])


# ------------------------------
# process raw data
# ------------------------------

cora_cites_path = r"../../datasets/planetoid/raw/cora/cora.cites"
cora_content_path = r"../../datasets/planetoid/raw/cora/cora.content"
citeseer_cites_path = r"../../datasets/planetoid/raw/citeseer/citeseer/citeseer.cites"
citeseer_content_path = r"../../datasets/planetoid/raw/citeseer/citeseer/citeseer.content"
pub_cites_path = r"../../datasets/planetoid/raw/Pubmed-Diabetes/data/Pubmed-Diabetes.DIRECTED.cites.tab"
pub_papers_path = r"../../datasets/planetoid/raw/Pubmed-Diabetes/data/Pubmed-Diabetes.NODE.paper.tab"


def load_cora_citeseer(name, content_path, cites_path):
    data = CitationData()

    papers = csv_util.read_csv(content_path, has_head=False, delimiter="\t")
    total = len(papers)
    count = 0
    for paper_info in papers:
        data.add_paper(f"{name}_{paper_info[0]}", paper_info[-1])
        count += 1
        process_bar.process_bar(count, total)

    cites = csv_util.read_csv(cites_path, has_head=False, delimiter="\t")
    total = len(cites)
    count = 0
    for [cited, citation] in cites:
        data.add_citation(f"{name}_{cited}", f"{name}_{citation}")
        count += 1
        process_bar.process_bar(count, total)

    pickle_util.dump_object(data, r"../../datasets/planetoid/%s.dataset" % name)


def load_pub():
    data = CitationData()
    papers = csv_util.read_csv(pub_papers_path, has_head=True, delimiter="\t")
    total = len(papers)
    count = 0
    for paper_info in papers[1:]:
        data.add_paper(f"pub_{paper_info[0]}", paper_info[1])
        count += 1
        process_bar.process_bar(count, total)

    cites = csv_util.read_csv(pub_cites_path, has_head=True, delimiter="\t")
    total = len(cites)
    count = 0
    for cite in cites[1:]:
        cited = cite[1].replace("paper:", "")
        citation = cite[3].replace("paper:", "")
        data.add_citation(f"pub_{cited}", f"pub_{citation}")
        count += 1
        process_bar.process_bar(count, total)

    pickle_util.dump_object(data, r"../../datasets/planetoid/raw/pub.dataset")


# ------------------------------
# extract commontype and transcites
# ------------------------------


def extract_commontype(data_name, data: CitationData):

    # 抽取 Type(x) := Type(y) AND Cited(x, y)
    results = []
    for paper_id in data.paper_citation:
        citations = data.paper_citation[paper_id]
        for citation in citations:
            if paper_id in data.papers and citation in data.papers:
                paper_type = data.papers[paper_id][0]
                citation_type = data.papers[citation][0]
                if paper_type == citation_type:
                    results.append([paper_id, citation, paper_type])

    csv_util.dump_csv(r"../../datasets/planetoid/preprocessed/%s.commontype.csv" % data_name,
                      rows=results, head=["paper", "citation", "type"])


def extract_transitive_citations(data_name, data: CitationData):
    # 抽取 Cited(x, z) := Cited(x, y) AND Cited(y, z)
    results = []
    for x_id in data.paper_citation:
        y_citations = data.paper_citation[x_id]
        for y_id in y_citations:
            if y_id in data.paper_citation:
                z_citations = data.paper_citation[y_id]
                for z_id in z_citations:
                    if z_id in y_citations:
                        if x_id in data.papers and y_id in data.papers and z_id in data.papers:

                            results.append([x_id, data.papers[x_id][0],
                                            y_id, data.papers[y_id][0],
                                            z_id, data.papers[z_id][0]])

    csv_util.dump_csv(r"../../datasets/planetoid/preprocessed/%s.transcite.csv" % data_name,
                      rows=results, head=["paper_x", "x_type", "paper_y", "y_type", "paper_z", "z_type"])


if __name__ == '__main__':

    # load_cora_citeseer("cora", cora_content_path, cora_cites_path)
    # load_cora_citeseer("citeseer", citeseer_content_path, citeseer_cites_path)
    # load_pub()

    data_name = "pub"
    dataset = pickle_util.load_object(r"../../datasets/planetoid/%s.dataset" % data_name)
    extract_commontype(data_name, dataset)
    extract_transitive_citations(data_name, dataset)



"""
# if __name__ == '__main__':
#     read_file = open(r"../../datasets/planetoid/ind.cora.y", "rb")
#     data = pickle.load(read_file, encoding='iso-8859-1')
#     read_file.close()
#     print(data)
"""
