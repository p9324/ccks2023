# coding = utf-8
from zzqlib.io import csv_util
import re


date_pattern = re.compile(r'([\d]{4})-([\d]+)-([\d]+)')


class DataSet(object):
    """
    the class for ext loading and saving.
    """

    def __init__(self):
        # original ext
        self.timeline = []
        self.data = []
        self.fields = []  # its a list

    """
    LOADING DATA
    """

    def load_data(self, data_path, offset_dict=None, used_fields=None):
        """load csv ext"""

        if not offset_dict:
            offset_dict = {
                "date": 0,
                "open": 1,
                "high": 2,
                "low": 3,
                "close": 4
            }

        if not used_fields:
            used_fields = ["open", "high", "low", "close"]

        self.fields = used_fields
        csv_rows = csv_util.read_csv(data_path, has_head=True)
        for csv_row in csv_rows:
            data_row = []
            self.timeline.append(csv_row[offset_dict['date']].replace('/', '-', 3))
            for field in used_fields:
                data_row.append(float(csv_row[offset_dict[field]]))
            if 0 in data_row:
                pass
            else:
                self.data.append(data_row)

    def is_empty(self):

        if len(self.timeline) == 0:
            return True
        return False

    """
    GETTING DATES.
    """

    def get_date(self, offset):
        return self.timeline[offset]

    def get_date_offset(self, date):
        return self.timeline.index(date)

    def get_date_offsets(self, base_date, start_date, end_date=-1, width=None):
        """获取起止日期偏移"""
        base_offset = self.timeline.index(base_date)
        start_index = self.timeline.index(start_date)

        if width is not None:
            return [i + start_index - base_offset for i in range(width)]
        elif end_date is not None:
            if end_date == -1:
                end_date = self.timeline[-1]
            end_index = self.timeline.index(end_date)
            return [i - base_offset for i in range(start_index, end_index + 1)]
        else:
            return None

    def get_dates(self, start_date, end_date):
        """获取日期本身"""
        start_index = self.timeline.index(start_date)
        end_index = self.timeline.index(end_date)
        return [self.timeline[i] for i in range(start_index, end_index + 1)]

    def available_trade_date(self, target_date):
        """获取同一月度，目标日期（如果目标日期是交易日），的下一个交易日"""

        target_obj = date_pattern.match(target_date)
        target_year = int(target_obj.group(1))
        target_month = int(target_obj.group(2))
        target_day = int(target_obj.group(3))

        for date in self.timeline:
            date_obj = date_pattern.match(date)
            current_year = int(date_obj.group(1))
            current_month = int(date_obj.group(2))
            current_day = int(date_obj.group(3))
            if current_year == target_year \
                    and current_month == target_month \
                    and current_day >= target_day:
                return date

    """
    GETTING DATA.
    """

    def get_batch_data(self, start_date, end_date, fields=None):
        """根据起止日期，以及fields获取部分数据"""

        start_index = self.timeline.index(start_date)
        end_index = self.timeline.index(end_date)

        # sub_timeline = self.timeline[start_index: end_index + 1]
        sub_data = self.data[start_index: end_index + 1]

        if fields is not None:
            new_data = []
            for d in sub_data:
                new_row = []
                for field in fields:
                    field_offset = self.fields.index(field)
                    if len(fields) > 1:
                        new_row.append(d[field_offset])
                    elif len(fields) == 1:
                        # 只获取一个字段的话去掉内部的[]
                        new_row = d[field_offset]
                new_data.append(new_row)
            sub_data = new_data

        return sub_data

    def get_value(self, date, field):
        """获取单个值"""
        date_offset = self.timeline.index(date)
        field_offset = self.fields.index(field)
        return self.data[date_offset][field_offset]
