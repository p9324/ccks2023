# encoding = utf-8


class Zenner(object):

    def __init__(self, logger):
        self.logger = logger
        self.zen_pipeline = []

    def boot(self, pipeline_handler_list):

        for proc in pipeline_handler_list:
            proc_initialized = proc(self.logger)
            self.zen_pipeline.append(proc_initialized)
        self.logger.info("[Zenner] pipeline initialized")

    def process(self, data_slice):
        """
        :param data_slice:
        :return:
        """

        self.logger.debug("[Zenner] %s process slice %s" % ("=" * 20, "=" * 20))
        data = data_slice
        for processor in self.zen_pipeline:
            data = processor.process(data)
            if "_status" in data:
                if not data["_status"]:
                    self.logger.error("[Zenner] backtest failed")
                    break
        data_slice = data
        return data_slice
