# coding = utf-8
import matplotlib.pyplot as plt
import numpy as np


def draw_nets(stgy_nets, bench_nets, date_labels):

    # 获取所需数据
    x_labels = date_labels
    x = [i for i in range(len(date_labels))]

    # 绘制策略净值曲线
    plt.plot(x, stgy_nets, color="blue", linewidth=1.5, linestyle="-", label="Zen")

    # 绘制基准净值曲线
    plt.plot(x, bench_nets, color="red", linewidth=1.5, linestyle="-", label="Benchmark")

    # 设置x轴的标签
    x_selected = list(np.linspace(0, len(date_labels) - 1, 10, endpoint=True).astype(int))
    x_label_selected = []
    for index_selected in x_selected:
        x_label_selected.append(x_labels[index_selected])
    plt.xticks(x_selected, x_label_selected, rotation=24)

    # 设置图例
    plt.legend(loc='upper right', frameon=False)
    plt.show()
