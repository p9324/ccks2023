# encoding = utf-8
from zzqlib.quant.zen.core import Zenner
from zzqlib.quant import data_set
from zzqlib.quant.zen.util import process_bar
from zzqlib.quant.zen.draw import pict_tool
from zzqlib.quant.zen.eval import generating


class StgyDayLine(object):

    def __init__(self, logger):
        self.logger = logger
        self.dataset = None
        self.zenner = Zenner(logger)
        self.stgy_conf = {}
        self.stgy_data = {
            "_slices": [],
            "_status": False
        }
        self.stgy_outputer = generating.ResultGenerator(logger)

    def boot(self, conf_dict):

        self.stgy_conf = conf_dict
        self.logger.info("[Zenner] configuration loaded")

        self.zenner.boot(self.stgy_conf["_pipeline"])

        self.dataset = data_set.DataSet()
        self.dataset.load_data(
            self.stgy_conf["_path"]["data_path"],
            offset_dict=self.stgy_conf["_path"]["field_offset_dict"],
            used_fields=self.stgy_conf["_path"]["used_fields"]
        )
        self.logger.info("[Zenner] data loaded from %s" % self.stgy_conf["_path"]["data_path"])

    """
    BACKTESTING
    """

    def backtest(self, print_process=False):
        """
        backtesing
        :param print_process:
        :return:
        """

        self.logger.info("[Zenner] %s backtesting start %s" % ("="*10, "="*10))

        start_date = self.stgy_conf["_args"]["start_date"]
        end_date = self.stgy_conf["_args"]["end_date"]

        if start_date == "null":
            start_date = self.dataset.get_date(0)
            self.stgy_conf["_args"]["start_date"] = start_date

        if end_date == "null":
            # 如果没有指明终止日期，那么就取最后一个
            end_date = self.dataset.get_date(-1)
            self.stgy_conf["_args"]["end_date"] = end_date

        k_dates = self.dataset.get_dates(start_date=start_date, end_date=end_date)
        k_data = self.dataset.get_batch_data(start_date=start_date, end_date=end_date)

        total_count = len(k_data)
        opt_history = []

        for i in range(0, total_count):

            # build slice
            data_slice = {
                "_status": False,
                "k_data": k_data[0: i+1],
                "k_dates": k_dates[0: i+1],
                "_args": {
                    "interval": self.stgy_conf["_args"]["interval"],
                    "min_range": self.stgy_conf["_args"]["min_range"]
                }
            }

            # run pipeline
            data_slice = self.zenner.process(data_slice)

            # record history
            opt_history.append({
                "opt_type": data_slice["opt_type"],
                "opt_offset": data_slice["opt_offset"],
                "opt_desc": data_slice["opt_desc"],
            })

            data_slice["opt_history"] = opt_history.copy()
            self.stgy_data["_slices"].append(data_slice)

            if print_process:
                process_bar.process_bar(i, total_count)

        if print_process:
            process_bar.process_bar(total_count, total_count)
            print("\n")

        self.stgy_data["_status"] = True
        self.logger.info("[Zenner] backtesting done")
        return

    """
    PROCESS RESULTS
    """

    def show_slice(self, data_slice_index=-1, show_adj_k=False, show_line=True, show_pivot=True, show_opt=True):
        """
        :param data_slice_index:
        :param show_adj_k:
        :param show_line:
        :param show_pivot:
        :param show_opt:
        :param window:
        :return:
        """

        if not self.stgy_data["_status"]:
            self.logger.error("[Zenner] backtesting undone")
            return

        data_slice_list = self.stgy_data["_slices"]
        data_slice = data_slice_list[data_slice_index]
        pict_tool.draw(data_slice, show_adj_k=show_adj_k, show_line=show_line, show_pivot=show_pivot, show_opt=show_opt)

    def generate_slice(self, data_slice_index, pict_path, show_adj_k=False, show_line=True, show_pivot=True, show_opt=True):
        """
        :param data_slice_index:
        :param pict_path:
        :param show_adj_k:
        :param show_line:
        :param show_pivot:
        :param show_opt:
        :return:
        """

        if not self.stgy_data["_status"]:
            self.logger.error("[Zenner] backtesting undone")
            return

        data_slice_list = self.stgy_data["_slices"]
        data_slice = data_slice_list[data_slice_index]
        pict_tool.save(data_slice, pict_path, show_adj_k=show_adj_k, show_line=show_line, show_pivot=show_pivot, show_opt=show_opt)
        self.logger.info("[Zenner] slice drawn and saved in %s" % pict_path)

    def generate_slices(self, html_path):
        """
        :param html_path:
        :return:
        """

        if not self.stgy_data["_status"]:
            self.logger.error("[Zenner] backtesting undone")
            return

        data_slices = self.stgy_data["_slices"]
        fig, ax = pict_tool.new_ax()
        pict_tool.draw_dynamic_zen(fig, ax, data_slices, html_path=html_path)

    def opt_desc(self):

        if not self.stgy_data["_status"]:
            self.logger.error("[Zenner] backtesting undone")
            return

        data_slices = self.stgy_data["_slices"]
        opt_desc = ''
        for data_slice in data_slices:
            opt_desc += '%s\t%s\n' % (data_slice["opt_offset"], data_slice["opt_desc"])
        return opt_desc

    """
    OUTPUT
    """

    def generate_opts(self, signal_path):

        if not self.stgy_data["_status"]:
            self.logger.error("[Zenner] backtesting undone")
            return

        data_slice = self.stgy_data["_slices"][-1]
        k_data = data_slice["k_data"]
        k_dates = data_slice["k_dates"]
        opt_history = data_slice["opt_history"]
        opt_type = []

        for opt_dict in opt_history:
            opt_type.append(opt_dict["opt_type"])

        self.stgy_outputer.generate_opts(k_data, k_dates, signal_path, opt_type)
