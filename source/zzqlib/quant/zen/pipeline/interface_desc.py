# encoding = utf-8

data_ori = {
    "_status": False or True,
    "_args": {
        "interval": 1,
        "min_range": 0.02
    },
    "k_data": [],   # shape(#tickers, 4), "open, high, low, close"
    "k_dates": []
}

data_cal_containment = {
    "_status": False or True,
    "_args": {
        "interval": 1,
        "min_range": 0.02
    },
    "k_data": [],   # shape(#tickers, 4), "open, high, low, close"
    "k_dates": [],

    "k_adj_data": []      # shape(#tickers, 4), adjusted k ext, "open, high, low, close"
}

data_cal_fractal = {
    "_status": False or True,
    "_args": {
        "interval": 1,
        "min_range": 0.02
    },
    "k_data": [],  # shape(#tickers, 4), "open, high, low, close"
    "k_dates": [],
    "k_adj_data": [],  # shape(#tickers, 4), adjusted k ext, "open, high, low, close"

    "trend": 1 or -1 or 0,
    "fractal_type": [],     # 分型的类型
    "fractal_offset": []   # 分型的偏移
}

data_cal_pivot = {
    "_status": False or True,
    "_args": {
        "interval": 1,
        "min_range": 0.02
    },
    "k_data": [],  # shape(#tickers, 4), "open, high, low, close"
    "k_dates": [],
    "k_adj_data": [],  # shape(#tickers, 4), adjusted k ext, "open, high, low, close"
    "trend": 1 or -1 or 0,
    "fractal_type": [],     # 分型的类型
    "fractal_offset": [],   # 分型的偏移

    "pivots": [...]
}

"""
shape(#numOfPivots), each value is a dict that is as follows:
        {
            "left":     0,  # left offset of tickers
            "right":    0,  # right offset of tickers
            "low":      0,  # low price of pivot
            "high":     0,  # high price of pivot
            "top":      0,  # top price of pivot
            "bottom":   0,  # bottom price of pivot
            "fractals": []  # fractals' offsets of tickers
        }
"""

data_cal_opt_ref = {
    "_status": False or True,
    "_args": {
        "interval": 1,
        "min_range": 0.02
    },
    "k_data": [],  # shape(#tickers, 4), "open, high, low, close"
    "k_dates": [],
    "k_adj_data": [],  # shape(#tickers, 4), adjusted k ext, "open, high, low, close"
    "trend": 1 or -1 or 0,
    "fractal_type": [],     # 分型的类型
    "fractal_offset": [],   # 分型的偏移
    "pivots": [...],

    "opt_ref": {
        "current_k_desc": ...,          # 当前k线类型描述
        "current_k_type": ...,          # 当前k线类型：0上笔延续点，1下笔延续点，2顶分型确认点，3底分型确认点，4单k
        "current_k_close": ...,
        "current_k_high": ...,
        "current_k_low": ...,
        "last_k_close": ...,            # 如果没有同current_k_close
        "last_k_high": ...,             # 如果没有同current_k_high
        "last_k_low": ...,              # 如果没有同current_k_low
        "current_pre_high": ...,        # 前高，可为None
        "current_pre_low": ...,         # 前低，可为None
        "last_pre_high": ...,           # 次前高，可为None
        "last_pre_low": ...,            # 次前低，可为None
        "current_pivot_high": ...,      # 次前低，可为None
        "current_pivot_low": ...,       # 可为None
        "current_pivot_top": ...,       # 可为None
        "current_pivot_bottom": ...,    # 可为None
        "last_pivot_high": ...,         # 可为None
        "last_pivot_low": ...,          # 可为None
        "last_pivot_top": ...,          # 可为None
        "last_pivot_bottom": ...       # 可为None
    }
}

data_cal_opt = {
    "_status": False or True,
    "_args": {
        "interval": 1,
        "min_range": 0.02
    },
    "k_data": [],           # shape(#tickers, 4), "open, high, low, close"
    "k_dates": [],
    "k_adj_data": [],       # shape(#tickers, 4), adjusted k ext, "open, high, low, close"
    "trend": 1 or -1 or 0,
    "fractal_type": [],     # 分型的类型
    "fractal_offset": [],   # 分型的偏移
    "pivots": [...],
    "opt_ref": {
        "current_k_desc": ...,          # 当前k线类型描述
        "current_k_type": ...,          # 当前k线类型：0上笔延续点，1下笔延续点，2顶分型确认点，3底分型确认点，4单k
        "current_k_close": ...,
        "current_k_high": ...,
        "current_k_low": ...,
        "last_k_close": ...,            # 如果没有同current_k_close
        "last_k_high": ...,             # 如果没有同current_k_high
        "last_k_low": ...,              # 如果没有同current_k_low
        "current_pre_high": ...,        # 前高，可为None
        "current_pre_low": ...,         # 前低，可为None
        "last_pre_high": ...,           # 次前高，可为None
        "last_pre_low": ...,            # 次前低，可为None
        "current_pivot_high": ...,      # 次前低，可为None
        "current_pivot_low": ...,       # 可为None
        "current_pivot_top": ...,       # 可为None
        "current_pivot_bottom": ...,    # 可为None
        "last_pivot_high": ...,         # 可为None
        "last_pivot_low": ...,          # 可为None
        "last_pivot_top": ...,          # 可为None
        "last_pivot_bottom": ...       # 可为None
    },

    "opt_type": ...,    # 操作类型，0持续，1买入，-1卖出
    "opt_offset": ...,  # 操作偏移
    "opt_desc": ...,    # 操作描述
}
