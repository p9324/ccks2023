# encoding = utf-8


open_offset = 0
high_offset = 1
low_offset = 2
close_offset = 3


class CalFractal(object):

    def __init__(self, logger):
        self.logger = logger

    """
    Input:
    {
        "_status": ...
        "_args": {"interval": 1, "min_range": 0.02}
        "k_data": ...
        "k_adj_data": ...
        "trend": ...
    }
    
    Output:
    {
        "_status": ...
        "_args": {"interval": 1, "min_range": 0.02}
        "k_data": ...
        "k_adj_data": ...
        "trend": ...
        "fractal_type": [...]     # 分型的类型
        "fractal_offset": [...]   # 分型的偏移
    }
    """
    def process(self, data):

        if "k_adj_data" not in data:
            self.logger.error("[CalFractal] \"k_adj_data\" not in data")
            data["_status"] = False
            return data

        if "trend" not in data:
            self.logger.error("[CalFractal] \"trend\" not in data")
            data["_status"] = False
            return data

        if "_args" not in data:
            self.logger.error("[CalFractal] \"_args\" not in data")
            data["_status"] = False
            return data

        if "interval" not in data["_args"]:
            self.logger.error("[CalFractal] \"interval\" not in data[\"_args\"]")
            data["_status"] = False
            return data

        k_data = data["k_data"]
        k_adj_data = data["k_adj_data"]
        trend = data["trend"]

        temp_offset = 0                            # 上一个候选分型的位置
        temp_type = 0                              # 上一个候选分型的类型

        interval = data["_args"]["interval"]       # 分型之间的间隔
        next_fractal_offset = interval + 1
        fractal_type = []                          # 记录分型点的类型，1为顶分型，-1为底分型, 0为起止点
        fractal_offset = []                        # 分型的偏移

        fractal_type.append(0)                     # 首先加上起点
        fractal_offset.append(0)                   # 起点的偏移

        i = 1                                      # 如果只有1根或2根k线，那么下面的while循环体不会进入
        has_unprocessed = False                    # 是否有未处理的新产生的候选分型
        while i < len(k_adj_data) - 1:
            """从前往后找分型，到倒数第2根k线"""

            current_data = k_adj_data[i]
            current_trend = trend[i]
            next_trend = trend[i+1]

            # current_is_top = previous_data[high_offset] <= current_data[high_offset] \
            #     and current_data[high_offset] > next_data[high_offset]
            # current_is_bottom = previous_data[low_offset] >= current_data[low_offset] \
            #     and current_data[low_offset] < next_data[low_offset]

            current_is_top = (current_trend == 1 and next_trend == -1)
            current_is_bottom = (current_trend == -1 and next_trend == 1)

            if current_is_top:
                # 目前的k线为顶分型

                if temp_type == 1:
                    # 如果上一个分型为顶分型，则进行比较，选取高点更高的分型:
                    if current_data[high_offset] < k_adj_data[temp_offset][high_offset]:
                        # 保持前k为顶分型
                        i += 1
                    else:
                        temp_offset = i
                        temp_type = 1
                        has_unprocessed = True
                        i += next_fractal_offset

                elif temp_type == -1:
                    # 如果上一个分型为底分型，则记录上一个分型，用当前分型与后面的分型比较，选取同向更极端的分型

                    if k_adj_data[temp_offset][low_offset] >= current_data[high_offset]:
                        # 如果上一个底分型的底比当前顶分型的顶高，则跳过当前顶分型.
                        i += 1
                    else:
                        # 如果上一个底分型的底比当前顶分型的顶低，确认上一个底分型成立.
                        fractal_type.append(temp_type)
                        fractal_offset.append(temp_offset)
                        temp_offset = i
                        temp_type = 1
                        has_unprocessed = True
                        i += next_fractal_offset

                elif temp_type == 0:  # 初始情况
                    temp_offset = i
                    temp_type = 1
                    has_unprocessed = True
                    i += next_fractal_offset

            elif current_is_bottom:
                # 当前k线判断出为底分型

                if temp_type == -1:
                    # 如果上一个分型为底分型，则进行比较，选取低点更低的分型
                    if current_data[low_offset] > k_adj_data[temp_offset][low_offset]:
                        i += 1
                    else:
                        temp_offset = i
                        temp_type = -1
                        has_unprocessed = True
                        i += next_fractal_offset

                elif temp_type == 1:
                    # 如果上一个分型为顶分型，则记录上一个分型，用当前分型与后面的分型比较，选取同向更极端的分型
                    if k_adj_data[temp_offset][high_offset] <= current_data[low_offset]:
                        # 如果上一个顶分型的顶比当前底分型的底低，则跳过当前底分型
                        i += 1
                    else:
                        # 如果上一个顶分型的顶比当前底分型的底高，确认上一个底分型成立.
                        fractal_type.append(temp_type)
                        fractal_offset.append(temp_offset)
                        temp_offset = i
                        temp_type = -1
                        has_unprocessed = True
                        i += next_fractal_offset
                elif temp_type == 0:
                    # 初始情况
                    temp_offset = i
                    temp_type = -1
                    has_unprocessed = True
                    i += next_fractal_offset

            else:
                i += 1

        if has_unprocessed:
            # 加上最后一个分型（上面的循环中最后的一个分型并未处理）
            fractal_type.append(temp_type)
            fractal_offset.append(temp_offset)

        if len(k_adj_data) > 1:
            # 至少有两根k线的情况，加上最后一根k线作为终点
            if temp_offset < len(k_adj_data) - 1:
                # 如果最后一根k线不是分型，那么得加上最后一根k线的二分位点作为线段终点
                fractal_type.append(0)
                fractal_offset.append(len(k_adj_data) - 1)

        fractal_type = fractal_type
        fractal_offset = fractal_offset

        data["fractal_type"] = fractal_type
        data["fractal_offset"] = fractal_offset
        data["_status"] = True
        return data
