# encoding = utf-8


# 默认都是“开、高、低、收”
open_offset = 0
high_offset = 1
low_offset = 2
close_offset = 3


class CalOptReference(object):

    def __init__(self, logger):
        self.logger = logger

    def process(self, data):

        if "fractal_type" not in data:
            self.logger.error("[CalOptRef] \"fractal_type\" not in data")
            data["_status"] = False
            return data

        if "fractal_offset" not in data:
            self.logger.error("[CalOptRef] \"fractal_offset\" not in data")
            data["_status"] = False
            return data

        if "k_data" not in data:
            self.logger.error("[CalOptRef] the field \"k_data\" is not in data")
            data["_status"] = False
            return data

        if "k_adj_data" not in data:
            self.logger.error("[CalOptRef] the field \"k_adj_data\" is not in data")
            data["_status"] = False
            return data

        if "pivots" not in data:
            self.logger.error("[CalOptRef] the field \"pivots\" is not in data")
            data["_status"] = False
            return data

        data["opt_ref"] = {}

        opt_ref_finders = [
            self._find_current_k,
            self._find_current_k_type,
            self._find_last_k,
            self._find_pivot,
            self._find_pre_ref
        ]

        for finder in opt_ref_finders:
            data = finder(data)

        data["_status"] = True
        return data

    def _find_current_k(self, data):

        k_data = data["k_data"]
        current_k = k_data[-1]

        data["opt_ref"]["current_k_close"] = current_k[close_offset]
        data["opt_ref"]["current_k_high"] = current_k[high_offset]
        data["opt_ref"]["current_k_low"] = current_k[low_offset]

        return data

    def _find_current_k_type(self, data):

        k_data = data["k_data"]
        fractal_type = data["fractal_type"]
        fractal_offset = data["fractal_offset"]

        last_fractal_type = None
        last_fractal_offset = None
        if len(fractal_type) > 1:
            last_fractal_type = fractal_type[-2]
            last_fractal_offset = fractal_offset[-2]

        current_k_type = None
        current_k_desc = None

        if last_fractal_type is not None:
            """具有前1个分型，肯定至少有两个k线，第一根k线是假分型"""

            if last_fractal_offset == len(k_data) - 2 and last_fractal_offset != 0:
                # 当下这根k线为分型确认点：前1个分型就是倒数第2根k（要把第1根k去掉）
                if last_fractal_type == 1:
                    current_k_type = 2
                    current_k_desc = "顶分型确认点"
                elif last_fractal_type == -1:
                    current_k_type = 3
                    current_k_desc = "底分型确认点"
            elif last_fractal_offset < len(k_data) - 2 and last_fractal_offset != 0:
                # 当下这根k为笔延续点：前1个分型就在倒数第2根k之前（要把第1根k去掉）
                if last_fractal_type == 1:
                    current_k_type = 1
                    current_k_desc = "下笔延续点"
                elif last_fractal_type == -1:
                    current_k_type = 0
                    current_k_desc = "上笔延续点"
            elif last_fractal_offset == 0:
                first_close = k_data[0][close_offset]
                current_close = k_data[-1][close_offset]
                if current_close > first_close:
                    # 上笔延续点
                    current_k_type = 0
                    current_k_desc = "第一笔上笔延续点"
                else:
                    # 下笔延续点
                    current_k_type = 1
                    current_k_desc = "第一笔下笔延续点"
        else:
            # 前1个分型不存在，说明当下只有1根k
            current_k_type = 4
            current_k_desc = "单根k线"

        data["opt_ref"]["current_k_type"] = current_k_type
        data["opt_ref"]["current_k_desc"] = current_k_desc
        return data

    def _find_last_k(self, data):

        k_data = data["k_data"]

        if len(k_data) > 1:
            # 如果有两根k线以上那么将前一根k线的收盘点也加进去
            last_close = k_data[-2][close_offset]
            last_high = k_data[-2][high_offset]
            last_low = k_data[-2][low_offset]
        else:
            last_close = k_data[-1][close_offset]
            last_high = k_data[-1][high_offset]
            last_low = k_data[-1][low_offset]

        data["opt_ref"]["last_k_close"] = last_close
        data["opt_ref"]["last_k_high"] = last_high
        data["opt_ref"]["last_k_low"] = last_low

        return data

    def _find_pivot(self, data):

        pivots = data["pivots"]

        current_pivot = None
        last_pivot = None

        if len(pivots) > 0:
            current_pivot = pivots[-1]

        if len(pivots) > 1:
            last_pivot = pivots[-2]

        data["opt_ref"]["current_pivot_high"] = current_pivot["high"] if current_pivot else None
        data["opt_ref"]["current_pivot_low"] = current_pivot["low"] if current_pivot else None
        data["opt_ref"]["current_pivot_top"] = current_pivot["top"] if current_pivot else None
        data["opt_ref"]["current_pivot_bottom"] = current_pivot["bottom"] if current_pivot else None
        data["opt_ref"]["current_pivot_fractal_num"] = len(current_pivot["fractals"]) if current_pivot else None

        data["opt_ref"]["last_pivot_high"] = last_pivot["high"] if last_pivot else None
        data["opt_ref"]["last_pivot_low"] = last_pivot["low"] if last_pivot else None
        data["opt_ref"]["last_pivot_top"] = last_pivot["top"] if last_pivot else None
        data["opt_ref"]["last_pivot_bottom"] = last_pivot["bottom"] if last_pivot else None
        data["opt_ref"]["last_pivot_fractal_num"] = len(last_pivot["fractals"]) if last_pivot else None

        return data

    def _find_pre_ref(self, data):
        """寻找前高前低"""

        k_adj_data = data["k_adj_data"]
        fractal_type = data["fractal_type"]
        fractal_offset = data["fractal_offset"]

        pre_highs = []
        pre_lows = []

        next_fractal_type = None                        # 下一分型类型
        next_close = k_adj_data[-1][close_offset]       # 最后一根k线收盘价

        if len(fractal_type) > 1:
            # 去掉最后一根k开始往前遍历分型

            for i in range(len(fractal_type) - 2, -1, -1):
                current_fractal_type = fractal_type[i]
                current_fractal_offset = fractal_offset[i]
                current_fractal_close = k_adj_data[current_fractal_offset][close_offset]
                current_fractal_high = k_adj_data[current_fractal_offset][high_offset]
                current_fractal_low = k_adj_data[current_fractal_offset][low_offset]

                if current_fractal_type == 1:
                    # 顶分型
                    pre_highs.insert(0, current_fractal_high)
                    next_fractal_type = 1
                elif current_fractal_type == -1:
                    # 底分型
                    pre_lows.insert(0, current_fractal_low)
                    next_fractal_type = -1
                elif current_fractal_type == 0:
                    # 第1根k
                    if next_fractal_type == 1:
                        # 接下来的是顶分型
                        pre_lows.insert(0, current_fractal_low)
                    elif next_fractal_type == -1:
                        # 接下来的是底分型
                        pre_highs.insert(0, current_fractal_high)
                    elif next_fractal_type is None:
                        # 没有分型，只有第1根和最后1根两个k
                        if current_fractal_close > next_close:
                            pre_lows.insert(0, current_fractal_low)
                        else:
                            pre_highs.insert(0, current_fractal_high)

        current_pre_high = None
        current_pre_low = None
        last_pre_high = None
        last_pre_low = None

        if len(pre_highs) > 0:
            current_pre_high = pre_highs[-1]

        if len(pre_highs) > 1:
            last_pre_high = pre_highs[-2]

        if len(pre_lows) > 0:
            current_pre_low = pre_lows[-1]

        if len(pre_lows) > 1:
            last_pre_low = pre_lows[-2]

        data["opt_ref"]["current_pre_high"] = current_pre_high
        data["opt_ref"]["current_pre_low"] = current_pre_low
        data["opt_ref"]["last_pre_high"] = last_pre_high
        data["opt_ref"]["last_pre_low"] = last_pre_low

        return data
