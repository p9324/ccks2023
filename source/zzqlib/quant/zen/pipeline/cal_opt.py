# encoding = utf-8


"""
[+] 2019.06.22 三类买卖点用中枢第一个分型
[+] 2019.06.28  突破型买点要突破中枢且突破前高，去掉突破中枢未突破前高的情况
"""


# 默认都是“开、高、低、收”
open_offset = 0
high_offset = 1
low_offset = 2
close_offset = 3


class CalOperation(object):

    def __init__(self, logger):
        self.logger = logger

    def process(self, data):

        if "opt_ref" not in data:
            self.logger.error("[CalOperation] \"opt_ref\" not in data")
            data["_status"] = False
            return data

        current_k_type = data["opt_ref"]["current_k_type"]
        k_data = data["k_data"]
        
        if current_k_type == 4:
            # 单根k
            self._opt_buy(data, opt_desc="第1根k：买入")

        elif current_k_type == 0:
            # 下笔延续点
            satisfied = self._opt_breakdown(data)
            if not satisfied:
                self._opt_breakup(data)

        elif current_k_type == 1:
            # 上笔延续点
            satisfied = self._opt_breakup(data)
            if not satisfied:
                self._opt_breakdown(data)

        elif current_k_type == 2:
            # 顶分型确认点
            satisfied = self._opt_second_kind_sell(data)
            if not satisfied:
                satisfied = self._opt_third_kind_sell(data)
            if not satisfied:
                self._opt_breakdown(data)

        elif current_k_type == 3:
            # 底分型确认点
            satisfied = self._opt_second_kind_buy(data)
            if not satisfied:
                satisfied = self._opt_third_kind_buy(data)
            if not satisfied:
                self._opt_breakup(data)

        if "opt_type" not in data:
            data["opt_type"] = 0
            data["opt_desc"] = "维持"

        data["opt_offset"] = len(k_data) - 1
        data["_status"] = True
        return data

    def _opt_buy(self, data, opt_desc):
        data["opt_type"] = 1
        data["opt_desc"] = opt_desc

    def _opt_sell(self, data, opt_desc):
        data["opt_type"] = -1
        data["opt_desc"] = opt_desc

    def _opt_hold(self, data, opt_desc):
        data["opt_type"] = 0
        data["opt_desc"] = opt_desc

    def _opt_breakup(self, data):
        """突破型买点"""

        current_k_close = data["opt_ref"]["current_k_close"]
        last_k_close = data["opt_ref"]["last_k_close"]
        current_pre_high = data["opt_ref"]["current_pre_high"]
        current_pivot_high = data["opt_ref"]["current_pivot_high"]

        if current_pre_high is not None and current_pivot_high is not None:
            if current_k_close > current_pre_high >= last_k_close and \
                    current_k_close > current_pivot_high >= last_k_close:
                # 首次突破前高，同时首次突破中枢顶
                self._opt_buy(data, opt_desc="首破前高及箱顶：买入")
                return True
            # if current_k_close > current_pivot_high >= last_k_close:
            #     self._opt_buy(data, opt_desc="首破箱顶未破前高：买入")
            #     return True

        return False

    def _opt_breakdown(self, data):
        """跌破型卖点"""
        current_k_close = data["opt_ref"]["current_k_close"]
        last_k_close = data["opt_ref"]["last_k_close"]
        current_pre_low = data["opt_ref"]["current_pre_low"]
        current_pivot_low = data["opt_ref"]["current_pivot_low"]

        if current_pre_low is not None and current_pivot_low is not None:
            if current_k_close < current_pre_low <= last_k_close and \
                    current_k_close < current_pivot_low <= last_k_close:
                # 同时跌破中枢底
                self._opt_sell(data, opt_desc="首破前低及箱底：卖出")
                return True
            # if current_k_close < current_pivot_low <= last_k_close:
            #     self._opt_sell(data, opt_desc="首破箱底未破前低：卖出")
            #     return True

        return False

    def _opt_second_kind_buy(self, data):
        """缠论第二类买点"""
        # last_k_close = data["opt_ref"]["last_k_close"]
        last_k_low = data["opt_ref"]["last_k_low"]
        last_pre_low = data["opt_ref"]["last_pre_low"]
        current_pivot_high = data["opt_ref"]["current_pivot_high"]

        if last_pre_low is not None:
            # 二类买点需要通过次前低对比
            if current_pivot_high is not None:
                if current_pivot_high > last_k_low >= last_pre_low:
                    # 二类买点，如果前k的收盘价突破中枢顶，那么前一日已经触发买点了
                    self._opt_buy(data, opt_desc="二类买点：买入")
                    return True
            else:
                if last_k_low >= last_pre_low:
                    self._opt_buy(data, opt_desc="二类买点：买入")
                    return True

        return False

    def _opt_second_kind_sell(self, data):
        """缠论第二类卖点"""
        # last_k_close = data["opt_ref"]["last_close"]
        last_k_high = data["opt_ref"]["last_k_high"]
        last_pre_high = data["opt_ref"]["last_pre_high"]
        current_pivot_low = data["opt_ref"]["current_pivot_low"]

        if last_pre_high is not None:
            # 二类买点需要找次高点来判断
            if current_pivot_low is not None:
                if current_pivot_low < last_k_high <= last_pre_high:
                    # 二类卖点，如果前k的收盘价跌破中枢底，那么前一日已经触发卖点了
                    self._opt_sell(data, opt_desc="二类卖点：卖出")
                    return True
            else:
                if last_k_high <= last_pre_high:
                    self._opt_sell(data, opt_desc="二类卖点：卖出")
                    return True
        return False

    def _opt_third_kind_buy(self, data):
        """缠论三类买点"""
        # last_k_close = data["opt_ref"]["last_k_close"]
        last_k_low = data["opt_ref"]["last_k_low"]
        last_pivot_high = data["opt_ref"]["last_pivot_high"]
        current_pivot_fractal_num = data["opt_ref"]["current_pivot_fractal_num"]

        if last_pivot_high is not None and current_pivot_fractal_num is not None:
            if last_k_low >= last_pivot_high and current_pivot_fractal_num <= 2:
                # 三类买点，前k低点大于等于前中枢高点且当前中枢至多有两个分型
                self._opt_buy(data, opt_desc="三类买点：买入")
                return True

        return False

    def _opt_third_kind_sell(self, data):
        """缠论三类卖点"""
        # last_k_close = data["opt_ref"]["last_k_close"]
        last_k_high = data["opt_ref"]["last_k_high"]
        last_pivot_low = data["opt_ref"]["last_pivot_low"]
        current_pivot_fractal_num = data["opt_ref"]["current_pivot_fractal_num"]

        if last_pivot_low is not None and current_pivot_fractal_num is not None:
            if last_k_high <= last_pivot_low and current_pivot_fractal_num <= 2:
                # 三类卖点，前k高点小于等于前中枢低点且当前中枢至多有两个分型
                self._opt_sell(data, opt_desc="三类卖点：卖出")
                return True

        return False
