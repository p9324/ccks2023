# encoding=utf-8


def format_dict(d, indent=''):
    formated_str = '{\n'
    if type(d) is dict:
        for d_key in d:
            d_value = d[d_key]
            if type(d_value) is dict:
                formated_str += indent + '\t\"' + d_key + '\": ' + format_dict(d[d_key], indent+'\t') + '\n'
            elif type(d_value) is list:
                l_str = '['
                current_index = 0
                for ele in d_value:
                    if type(ele) is not str:
                        l_str += '' + str(ele)
                        if current_index != len(d_value) - 1:
                            l_str += ', '
                    else:
                        l_str += '\"' + str(ele) + '\"'
                        if current_index != len(d_value) - 1:
                            l_str += ', '
                    current_index += 1
                l_str += ']'
                formated_str += indent + '\t\"' + d_key + '\": ' + l_str + ',\n'

            else:
                if type(d_value) is not str:
                    formated_str += indent + '\t\"' + d_key + '\": ' + str(d_value) + ',\n'
                else:
                    formated_str += indent + '\t\"' + d_key + '\": \"' + str(d_value) + '\",\n'

    formated_str += indent + '}'
    return formated_str


if __name__ == '__main__':

    dd = {
        "key1": "value1",
        "inner_key": {
            "key2": "value2",
            "inner_inner_key": {
                "key3": "value3",
                "iii_key": {
                    "key5": [1, 2,5,7]
                }
            },
            "key4": "value4"
        }
    }

    s = format_dict(dd, '')

    print(s)
