# encoding=utf-8

import logging


def get_logger(name, level, write_to_file=False, log_path=None):
    """
    return a logger that prints messages to file.
    """

    logger = logging.getLogger(name)
    logger.setLevel(level=level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    console_handler = logging.StreamHandler()
    console_handler.setLevel(level)
    console_handler.setFormatter(formatter)

    logger.addHandler(console_handler)

    if write_to_file:
        if log_path:
            file_handler = logging.FileHandler(log_path)
            file_handler.setLevel(level)
            file_handler.setFormatter(formatter)
            logger.addHandler(file_handler)
        else:
            raise Exception("the arg **log_path** should be given.")

    return logger


if __name__ == '__main__':

    logger = get_logger("test", logging.DEBUG, write_to_file=True, log_path='log.txt')
    logger.debug("%d people come in.", 23)


