# encoding=utf-8
import csv

"""
r: read texts
rb: read binaries (it is different from the type(byte) in python)
"""


def read_csv(csv_path, has_head, encoding='utf-8', delimiter=','):
    """
    load csv file.
    the encoding is utf-8 by default;
    the delimiter is ',' by default.
    """
    row_list = []
    with open(csv_path, 'r', encoding=encoding) as csv_file:
        lines = csv.reader(csv_file, delimiter=delimiter)

        reach_head = True
        for row in lines:
            # remove the head of the csv file.
            if reach_head and has_head:
                reach_head = False
                continue
            row_list.append(row)
    return row_list


def dump_csv(csv_path, head, rows, encoding='utf-8', delimiter=','):
    """
    dump csv as utf-8
    """
    # newline is used to separate lines.
    with open(csv_path, 'w', encoding=encoding, newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=delimiter)
        # write head.
        writer.writerow(head)
        # write rows.
        writer.writerows(rows)


def add_row_to(csv_path, row, encoding='utf-8', delimiter=','):
    """
    add a line to the existed csv file.
    """
    # newline is used to separate lines.
    with open(csv_path, "a+", encoding=encoding, newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=delimiter)
        writer.writerow(row)
