# encoding = utf-8
import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib.patches as patches
import mpl_finance as mpf
import numpy as np


# 默认都是“开、高、低、收”
OPEN_OFFSET = 0
HIGH_OFFSET = 1
LOW_OFFSET = 2
CLOSE_OFFSET = 3


"""
绘制
"""


def draw(data_slice, show_adj_k=False, show_line=True, show_pivot=True, show_opt=True, window=120):

    fig, ax = new_ax()

    k_adj_data = data_slice["k_adj_data"]
    k_data = data_slice["k_data"]
    fractal_type = data_slice["fractal_type"]
    fractal_offset = data_slice["fractal_offset"]
    pivots = data_slice["pivots"]
    opt_history = data_slice["opt_history"]
    opt_desc = data_slice["opt_desc"]

    if show_adj_k:
        draw_k(ax, k_adj_data)
    else:
        draw_k(ax, k_data)
    if show_line:
        draw_lines(ax, k_data, fractal_type, fractal_offset)
    if show_pivot:
        draw_pivots(ax, k_adj_data, pivots)
    if show_opt:
        draw_opts(ax, k_data, opt_history)
        draw_opt_desc(ax, k_data, opt_desc)

    show_pict()


def save(data_slice, pict_path, show_adj_k=False, show_line=True, show_pivot=True, show_opt=True, window=120):

    fig, ax = new_ax()

    k_adj_data = data_slice["k_adj_data"]
    k_data = data_slice["k_data"]
    fractal_type = data_slice["fractal_type"]
    fractal_offset = data_slice["fractal_offset"]
    pivots = data_slice["pivots"]
    opt_history = data_slice["opt_history"]
    opt_desc = data_slice["opt_desc"]

    if show_adj_k:
        draw_k(ax, k_adj_data)
    else:
        draw_k(ax, k_data)
    if show_line:
        draw_lines(ax, k_data, fractal_type, fractal_offset)
    if show_pivot:
        draw_pivots(ax, k_adj_data, pivots)
    if show_opt:
        draw_opts(ax, k_data, opt_history)
        draw_opt_desc(ax, k_data, opt_desc)

    fig.savefig(pict_path)


"""
画布初始化
"""


def new_ax():
    fig = plt.figure(figsize=(10, 6))
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    return fig, ax


def show_pict():
    plt.show()


"""
基本元素绘制
"""


def draw_k(ax, k_data):
    """绘制k线图"""
    num_of_dates = len(k_data)
    candle_data = np.column_stack([list(range(num_of_dates)), k_data])
    mpf.candlestick_ohlc(ax, candle_data, width=0.2, colorup='r', colordown='g')


def draw_lines(ax, k_data, fractal_type, fractal_offset, color='black'):
    """绘制笔"""
    fractal_dot = []
    for i in range(len(fractal_type)):
        f_type = fractal_type[i]
        offset = fractal_offset[i]
        data = k_data[offset]

        dot = 0
        if f_type == 0:
            dot = (data[OPEN_OFFSET] + data[CLOSE_OFFSET]) / 2.0
            # dot = data[close_offset]
        elif f_type == -1:
            dot = data[LOW_OFFSET]
            # dot = data[close_offset]
        elif f_type == 1:
            dot = data[HIGH_OFFSET]
            # dot = data[close_offset]

        fractal_dot.append(dot)

    ax.plot(fractal_offset, fractal_dot, 'k', lw=0.8, color=color)


def draw_pivots(ax, k_adj_data, pivots, window=120):
    """draw pivots"""
    for pivot in pivots:
        _draw_pivot(ax, k_adj_data, pivot, window)


def _draw_pivot(ax, k_adj_data, pivot, window=120):
    """
            pivot = {
                "left": (lbound, rbound),
                "right": (lbound, rbound),
                "low": 0,
                "high": 0,
                "top": 0,
                "bottom": 0,
                "fractals": []
            }
            """

    # pivot["left"] = (pivot["left"][0] + pivot["left"][1]) / 2
    # pivot["right"] = (pivot["right"][0] + pivot["right"][1]) / 2

    if len(k_adj_data) < window or window == -1:
        start_index = 0
    else:
        start_index = len(k_adj_data) - window
    if start_index > pivot["left"]:
        # if the pivot is not totaly in the figure, ignore it
        return
    else:
        pivot["right"] = pivot["right"] - start_index
        pivot["left"] = pivot["left"] - start_index

    small_start_point = (pivot["left"], pivot["low"])
    large_start_point = (pivot["left"], pivot["bottom"])
    width = pivot["right"] - pivot["left"]
    small_height = pivot["high"] - pivot["low"]
    large_height = pivot["top"] - pivot["bottom"]
    ax.add_patch(
        patches.Rectangle(
            small_start_point,  # (x,y)
            width,  # width
            small_height,  # height
            edgecolor='black',
            facecolor='none'
        )
    )

    ax.add_patch(
        patches.Rectangle(
            large_start_point,  # (x,y)
            width,  # width
            large_height,  # height
            edgecolor='blue',
            facecolor='none'
        )
    )


def draw_opts(ax, k_data, opt_history):

    sell_offsets = []
    sell_opts = []
    buy_offsets = []
    buy_opts = []

    for opt in opt_history:
        opt_type = opt["opt_type"]
        opt_offset = opt["opt_offset"]
        if opt_type == 1:
            buy_offsets.append(opt_offset)
            buy_opts.append(k_data[opt_offset][LOW_OFFSET])
        elif opt_type == -1:
            sell_offsets.append(opt_offset)
            sell_opts.append(k_data[opt_offset][HIGH_OFFSET])

    ax.plot(sell_offsets, sell_opts, 'v', color='darkgreen', ms=4)
    ax.plot(buy_offsets, buy_opts, '^', color='deeppink', ms=4)


def draw_opt_desc(ax, k_data, opt_desc):
    x = len(k_data) - 1
    y = k_data[-1][HIGH_OFFSET]
    ax.text(x, y, opt_desc, fontproperties="SimHei")


"""
缠论切片绘制
"""


def draw_dynamic_zen(fig, ax, zen_slices, html_path=r'../ext/core.html'):
    args = []
    for zen_slice in zen_slices:
        args.append((ax, zen_slice))
    _draw_slices(fig, args, html_path=html_path)


def _draw_slices(fig, args, html_path=r'../ext/core.html'):
    # blit=True func必须要返回东西才行
    ani = animation.FuncAnimation(fig, func=draw_slice, frames=args, interval=2000, blit=False)
    ani.save(html_path, writer='imagemagick', fps=100)


def draw_slice(arg):
    (ax, zen_slice) = arg
    # 清除之前画的
    ax.clear()
    # 绘制k线
    draw_k(ax, zen_slice["k_data"])
    # 绘制分型和线段
    draw_lines(ax, zen_slice["k_adj_data"], zen_slice["fractal_type"], zen_slice["fractal_offset"])
    # 绘制中枢
    draw_pivots(ax, zen_slice["k_adj_data"], zen_slice["pivots"], window=-1)
    # 绘制买卖点
    draw_opts(ax, zen_slice["k_data"], zen_slice["opt_history"])
    # 绘制买卖描述
    draw_opt_desc(ax, zen_slice["k_data"], zen_slice["opt_desc"])
