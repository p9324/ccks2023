# encoding=utf-8

from pyltp import SentenceSplitter
from pyltp import Segmentor
from pyltp import Postagger
from pyltp import NamedEntityRecognizer
from pyltp import Parser
from pyltp import SementicRoleLabeller

import logging
from zzqlib.log import log_util


class LTPUtil(object):
    # a class that packages the functions of ltp.

    def __init__(self):
        self.logger = log_util.get_logger("LTPUtil", logging.DEBUG)
        self.segmentor_default = None
        self.segmentor_usrdef = None
        self.postagger = None
        self.recognizer = None
        self.parser = None
        self.sementic_role_labeller = None

    def load_models(
            self,
            seg_model_path='cws.model',
            pos_model_path='pos.model',
            ner_model_path='ner.model',
            parser_model_path='parser.model',
            srl_model_path='srl.model'
    ):
        """
        load all models.
        """
        self.logger.debug("loading models.")

        # default segmentor
        if not self.segmentor_default:
            self.segmentor_default = Segmentor()
            self.segmentor_default.load(seg_model_path)
            self.logger.debug("\t the default segmentor model has been loaded, using model %s.",
                              seg_model_path)
        else:
            self.segmentor_default.release()
            self.segmentor_default.load(seg_model_path + r'cws.model')
            self.logger.debug("\t the default segmentor model has been reloaded.")

        # postagger
        if not self.postagger:
            self.postagger = Postagger()
            self.postagger.load(pos_model_path)
            self.logger.debug("\t the default postagger model has been loaded, using model %s.",
                              pos_model_path)
        else:
            self.postagger.release()
            self.postagger.load(pos_model_path)
            self.logger.debug("\t the default postagger model has been reloaded.")

        # ner
        if not self.recognizer:
           self.recognizer = NamedEntityRecognizer()
           self.recognizer.load(ner_model_path)
           self.logger.debug("\t the recognizer model has been loaded, using model %s.",
                             ner_model_path)
        else:
           self.recognizer.release()
           self.recognizer.load(ner_model_path)
           self.logger.debug("\t the recognizer model has been reloaded.")

        # parser
        if not self.parser:
            self.parser = Parser()
            self.parser.load(parser_model_path)
            self.logger.debug("\t the parser model has been loaded, using model %s.",
                             parser_model_path)
        else:
            self.parser.release()
            self.parser.load(parser_model_path)
            self.logger.debug("\t the parser model has been reloaded.")

        # labeller
        if not self.sementic_role_labeller:
            self.sementic_role_labeller = SementicRoleLabeller()
            self.sementic_role_labeller.load(srl_model_path)
            self.logger.debug("\t the semantic role labeller has been loaded using mode %s.",
                              srl_model_path)
        else:
            self.sementic_role_labeller.release()
            self.sementic_role_labeller.load(srl_model_path)
            self.logger.debug("\t the semantic role labeller has been reloaded.")

    def release_models(self):
        self.logger.debug("releasing models.")

        # default segmentor
        if self.segmentor_default:
            self.segmentor_default.release()
            self.logger.debug("\t the default segmentor model has been released.")

        # user defined segmentor
        if self.segmentor_usrdef:
            self.segmentor_usrdef.release()
            self.logger.debug("\t the segmentor with user dict has been released.")

        # release postagger
        if self.postagger:
            self.postagger.release()
            self.logger.debug("\t the postagger has been released.")

        # recognizer
        if self.recognizer:
            self.recognizer.release()
            self.logger.debug("\t the recognizer has been released.")

        # parser
        if self.parser:
            self.parser.release()
            self.logger.debug("\t the parser has been released.")

        # role labeller
        if self.sementic_role_labeller:
            self.sementic_role_labeller.release()
            self.logger.debug("\t the labeller has been released.")

    def load_dict(self, segmentor_model_path='', dict_path=''):
        self.logger.debug("loading user dict.")

        # user segmentor
        if not self.segmentor_usrdef:
            self.segmentor_usrdef = Segmentor()
            self.segmentor_usrdef.load_with_lexicon(segmentor_model_path, dict_path)
            self.logger.debug(
                "\t the segmentor with user dict has been loaded, using model in %s, using dict in %s.",
                segmentor_model_path, dict_path)
        else:
            self.segmentor_usrdef.release()
            self.segmentor_usrdef.load_with_lexicon(segmentor_model_path, dict_path)
            self.logger.debug(
                "\t the segmentor with user dict has been reloaded, using model in %s, using dict in %s.",
                segmentor_model_path, dict_path)

    def split(self, sentence):
        # split sentence
        if self.segmentor_default:
            pass
        return SentenceSplitter.split(sentence)

    def segment(self, sentence, default=True):
        # word segmentation
        if default:
            return self.segmentor_default.segment(sentence)
        else:
            return self.segmentor_usrdef.segment(sentence)

    def postag(self, words):
        """
        *words* should be list.
        """
        return self.postagger.postag(words)

    def recognize(self, words, postages):
        return self.recognizer.recognize(words, postages)

    def parse(self, words, postages):
        return self.parser.parse(words, postages)

    def label(self, words, postags, arcs):
        return self.sementic_role_labeller.label(words, postags, arcs)
