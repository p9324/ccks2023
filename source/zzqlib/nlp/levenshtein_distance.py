# encoding=utf-8


def cal_similarity(str1, str2):
    """
    calculate the similarity of str1 and str2 based on the levenshtein Distance.
    str1 and str2 should be the type of 'str' (unicode).
    """

    len1 = len(str1)
    len2 = len(str2)

    if max(len1, len2) == 0:
        return 1.0

    dist = []

    for i in range(len1 + 1):
        dist.append([0 for _ in range(len2 + 1)])

    for i in range(1, len1 + 1):
        dist[i][0] = i
    for j in range(1, len2 + 1):
        dist[0][j] = j

    for i in range(1, len1 + 1):
        for j in range(1, len2 + 1):
            if str1[i-1] == str2[j-1]:
                dist[i][j] = dist[i-1][j-1]
            else:
                dist[i][j] = min(dist[i-1][j-1], dist[i][j-1]+1, dist[i-1][j]+1)

    return 1 - (dist[len1][len2] / max(len1, len2))
