# encoding = utf-8
import time
import datetime
import sys


"""
TO STR
"""


def date2str():
    return time.strftime('%Y-%m-%d', time.localtime(int(time.time())))


def current_time():
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(time.time())))


def current_date():
    return time.strftime('%Y-%m-%d', time.localtime(int(time.time())))


def timestamp2str(timestamp):
    local = time.localtime(timestamp)
    str_time = time.strftime('%Y-%m-%d %H:%M:%S', local)
    return str_time


def beforeDaysStr(time_str, num):
    return dt2str(beforeDaysDt(str2dt(time_str), num))


def beforeDaysDateStr(date_str, num):
    return dt2str(beforeDaysDt(dateStr2dt(date_str), num))


def dt2str(dt):
    return dt.strftime("%Y-%m-%d")


def nextDayStr(time_str):
    return dt2str(nextDayDt(str2dt(time_str)))


def nextDayDateStr(date_str):
    return dt2str(nextDayDt(dateStr2dt(date_str)))


"""
TO TIMESTAMP
"""


def timestap():
    return int(time.time()*1000)


def time2timestap(dt):
    return int(time.mktime(dt.timetuple())*1000)


"""
TO DATETIME
"""

def beforeDaysDt(dt, num):
    one_data = datetime.timedelta(days=1)
    before_dt = dt
    for i in range(num):
        before_dt = before_dt - one_data
    return before_dt


def nextDayDt(dt):
    one_data = datetime.timedelta(days=1)
    next_dt = dt + one_data
    return next_dt


def str2dt(time_str):
    return datetime.datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")


def dateStr2dt(date_str):
    return datetime.datetime.strptime(date_str, "%Y-%m-%d")


"""
OTHERS
"""


def print_time():
    ct = current_time()
    sys.stdout.write('\r')
    sys.stdout.write(ct)
    sys.stdout.flush()


if __name__ == '__main__':
    # print(time2str())
    # print(date2str())
    # print(dt2str(nextDayDt(str2dt("2018-03-31"))))
    dt = timestamp2str(float(1553875200000)/1000)
    print(dt)
    # print(current_time())
    # print(beforeDaysStr('2018-03-12', 12))
    # print(timestamp2str(timestap()/1000))
    # print(time2timestap("2018-03-31 00:00:01"))
