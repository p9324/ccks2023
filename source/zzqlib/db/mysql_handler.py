# coding = utf-8
import mysql.connector


class MySQLTool(object):

    def __init__(self, logger):
        self.conn = None
        self.logger = logger
        self.host = None
        self.port = None
        self.user = None
        self.passwd = None
        self.database = None
        self.cursor = None

    def connect(self, host, port, user, passwd, database):
        """
        connect to mysql
        """
        try:
            if self.conn is not None and self.cursor is not None:
                self.close()
            self.conn = mysql.connector.connect(
                host=host,
                port=port,
                user=user,
                passwd=passwd,
                database=database
            )
            self.cursor = self.conn.cursor()
            self.host = host
            self.port = port
            self.user = user
            self.passwd = passwd
            self.database = database
            self.logger.info(
                "successfully connected to database %s at (%s:%d) on user \"%s\"" % (database, host, port, user))
        except Exception as e:
            self.logger.error("error occurs when connecting to database: %s" % str(e))

    def exist(self, target_tb_name):
        """
        check whether the target_tb_name exists
        :param target_tb_name:
        :return:
        """
        result = self.execute_with_result("show tables")
        for (tb_name,) in result:
            if tb_name == target_tb_name:
                return True
        return False

    def insert(self, sql, val):
        if self.cursor:
            self.cursor.execute(sql, val)
            self.conn.commit()
        else:
            raise Exception("cursor is not initialized")

    def insert_batch(self, sql, val_list):
        if self.cursor:
            self.cursor.executemany(sql, val_list)
            self.conn.commit()
            self.logger.debug("total %d records inserted" % len(val_list))
        else:
            raise Exception("cursor is not initialized")

    def execute(self, sql):
        if self.cursor:
            self.cursor.execute(sql)
        else:
            raise Exception("cursor is not initialized")

    def execute_with_commit(self, sql):
        if self.cursor:
            self.cursor.execute(sql)
            self.conn.commit()
        else:
            raise Exception("cursor is not initialized")

    def execute_with_result(self, sql):
        if self.cursor:
            self.cursor.execute(sql)
            result = self.cursor.fetchall()
            return result
        else:
            raise Exception("cursor is not initialized")

    def close(self):
        if self.cursor:
            self.cursor.close()
        if self.conn:
            self.conn.close()
        self.conn = None
        self.cursor = None
        self.logger.info("the database database (%s:%d) on user \"%s\" closed" % (self.host, self.port, self.user))
