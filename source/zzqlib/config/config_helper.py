# encoding=utf-8
import configparser


def load_config(conf_path, encoding):
    """
    read the configuration.
    :param conf_path:
    :param encoding:
    :return:
    """
    conf = configparser.ConfigParser()
    conf.read(conf_path, encoding=encoding)
    return conf


def read_conf(conf, sec, attr):
    """

    :param conf:
    :param sec:
    :param attr:
    :return:
    """
    return conf.get(sec, attr)


# if __name__ == '__main__':
#     conf = load_config('config.conf', 'utf-8')
#     value1 = read_conf(conf, 'section', 'v1')
#     value2 = read_conf(conf, 'section', 'v2')
#     print(value2)
