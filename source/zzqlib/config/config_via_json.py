# encoding=utf-8
import json
from zzqlib.io import file_helper


def read_conf_as_json(conf_path):
    """
    encoding is utf-8.
    """
    conf_text = file_helper.read_file_as_text(conf_path, encoding='utf-8')
    conf_dict = json.loads(conf_text)
    return conf_dict


if __name__ == '__main__':
    conf_dict = read_conf_as_json('../../automata/crobot_atm.json')
    print(conf_dict)
