# encoding=utf-8

from html.parser import HTMLParser
from zzqlib.io import file_helper as fh
import re


class HTMLParserUtil(HTMLParser):

    def __init__(self):
        super(HTMLParserUtil, self).__init__()
        self.sentence_list = []
        self.current_text = ''
        self.be_appended = False

    def handle_starttag(self, tag, attrs):

        if tag == 'div':
            self.be_appended = True

    def handle_endtag(self, tag):

        if tag == 'div':
            if self.current_text not in self.sentence_list:
                self.sentence_list.append(self.current_text)

            self.current_text = ''
            self.be_appended = False

    def handle_data(self, data):
        if self.be_appended:
            text = re.sub(r'\s', '', data.strip())
            self.current_text = self.current_text + text

    def extract_from(self, html_path, encoding='utf-8'):
        """
        extract all the information from html file,
        save them as lines in self.text_list.
        """
        html_text = fh.read_file_as_text(html_path, encoding=encoding)
        self.sentence_list = []
        self.feed(html_text)
        return self.sentence_list


if __name__ == '__main__':

    parser2 = HTMLParserUtil()
    parser2.extract_from(r'D:/quanzz/AILab/裁判文书/数据/data_test/裁判文书解析验证集/snample/1a0a716b-090b-4a88-8ec0-a87b00fac354.txt')

    for sentence in parser2.sentence_list:
        print(sentence)
