# encoding=utf-8
import pickle


def dump_object(obj, file_path):
    with open(file_path, 'wb') as f:
        pickle.dump(obj, f, protocol=3)


def load_object(file_path):
    with open(file_path, 'rb') as f:
        obj = pickle.load(f)
        return obj


"""
class PickleTest(object):

    def __init__(self):
        pass

    def show(self):
        print("i am pickle test")


if __name__ == '__main__':

    pickleTest = PickleTest()
    dump_object(pickleTest, r'../../data1/pickletest.pkl')
    testObj = load_object(r'../../data1/pickletest.pkl')
    testObj.show()
"""
