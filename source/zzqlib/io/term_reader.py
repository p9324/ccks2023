# encoding=utf-8

"""
用于读取术语文件.
"""
import re
from zzqlib.io import file_helper


def read_terms_as_utf8(file_path):
    """
    读取术语文件，“#”行为解释行，术语由“,”分隔
    """
    lines = file_helper.read_file_as_utf8(file_path)
    term_list = []
    for line in lines:

        # 不处理以#, \n, \r开头的行
        if line.startswith('#'):
            continue
        if line.startswith('\n'):
            continue
        if line.startswith('\r'):
            continue

        # 将空格类字符去除
        line = re.sub('\s', '', line)
        terms = line.split(',')

        # 将空字符去除
        for term in terms:
            if term != '':
                term_list.append(term)

    return term_list


# if __name__ == '__main__':
#
#     terms_list = read_terms_as_utf8('_example_terms.txt')
#     for term in terms_list:
#         print(term)
