# encoding = utf-8
from kafka import KafkaProducer


class KafkaProduceTool(object):
    """Kafka producer"""

    def __init__(self, broker_address, topic, logger):
        self.logger = logger
        self.broker_address = broker_address
        self.broker_address_list = broker_address.split(',')
        self.topic = topic
        self.kafka_producer = None
        try:
            self.kafka_producer = KafkaProducer(bootstrap_servers=self.broker_address_list)
            self.logger.info("a producer connected to Kafka on address (%s)" % broker_address)
        except Exception as e:
            self.logger.error(str(e))

    def produce(self, msg):
        """produce message"""
        if self.kafka_producer is not None:
            self.kafka_producer.send(self.topic, msg)
        else:
            self.logger.warn("the kafka producer is not initialized")

    def close(self):
        if self.kafka_producer is not None:
            self.kafka_producer.close()
            self.logger.info("the kafka producer is closed")
