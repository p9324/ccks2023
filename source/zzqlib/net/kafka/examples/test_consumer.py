# encoding = utf-8
from sf_kafka import consumer
from sf_util import log_util


def process_msg(msg):
    tpk = msg.topic
    part = msg.partition
    offset = msg.offset
    key = msg.key
    value = msg.value
    print("%s:%d:%d" % (tpk, part, offset))
    print(key.decode("utf-8"))
    print(value.decode("utf-8"))


if __name__ == '__main__':

    broker_addresses = \
        'kafkasit02broker01.cnsuning.com:9092,kafkasit02broker02.cnsuning.com:9092,kafkasit02broker03.cnsuning.com:9092'
    topic = "fbicics_yuqing_topic"
    group_id = "fbicics_yuqing_groupid_sit"
    # group_id = "sfnlp_yuqing_groupId_sit"

    import logging
    logger = log_util.get_logger("test", logging.DEBUG)

    kafkaConsumer = consumer.KafkaConsumeTool(
        broker_address=broker_addresses, topic=topic, group_id=group_id, logger=logger)

    kafkaConsumer.consume(process_msg, consume_limit=501)

    kafkaConsumer.close()



