# encoding=utf-8


def scan_socket(target_iP, target_Port):
    """
    :param targetIP: should be str
    :param target_Port: should be int
    :return [STATE_CODE(0,1), RESPONSE(str)]
    """
    import socket
    try:
        connSkt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connSkt.connect((target_iP, target_Port))
        connSkt.send(b'hello')
        print("waiting")
        response = connSkt.recv(100)
        return 1, str(response)
    except:
        return 0, 'null'


if __name__ == '__main__':

    for i in range(10, 3655):
        print("connecting to %d " % i)
        state, results = scan_socket('10.37.235.10', i)
        if state == 1:
            print(state, results)
