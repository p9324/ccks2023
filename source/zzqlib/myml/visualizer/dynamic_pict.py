# encoding=utf-8
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import numpy as np


class DynamicDots(object):

    def __init__(self, dots):
        self.fig, self.ax = plt.subplots()
        self.x_data = dots[0]
        self.y_data = dots[1]
        self.frame_index = [i for i in range(0, len(self.x_data))]
        self.ax.set_xlim(self.x_data.min(), self.x_data.max())
        self.ax.set_ylim(self.y_data.min(), self.y_data.max())

    def update_frame(self, n):
        x = self.x_data[0:n]
        y = self.y_data[0:n]
        dots, = self.ax.plot(x, y, 'ro', color='blue')
        return dots,

    def draw_dynamic(self):
        ani = FuncAnimation(self.fig, self.update_frame, frames=self.frame_index,
                            init_func=None, blit=True)
        plt.show()


def draw_diff(diff_list):

    # 用图形化的方法展示Diff的变化过程
    x_data = [i for i in range(len(diff_list))]
    x_data = np.array(x_data)
    y_data = np.array(diff_list)

    dy_dots = DynamicDots((x_data, y_data))
    dy_dots.draw_dynamic()

