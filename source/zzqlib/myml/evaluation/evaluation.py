# encoding=utf-8
from sklearn import metrics


class Evaluator(object):

    def __init__(self):
        pass

    @staticmethod
    def evaluate(real_classes, predicated_classes, label_index):
        """
        :param real_classes: np.ndarray(numofsamples,)
        :param predicated_classes: np.ndarray(numofsamples,)
        :param label_index: [label1, label2, ...]
        :return: str: report
        """
        return metrics.classification_report(
            real_classes, predicated_classes, target_names=label_index)
