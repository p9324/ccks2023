# encoding=utf-8
import logging
from zzqlib.time import date_time


def get_logger_by_date(name, level, log_dir, encoding='gbk'):
    import os
    current_dt = date_time.current_date()
    log_path = '%s/log_%s' % (log_dir, current_dt)
    if not os.path.exists(log_path):
        # create new file
        f = open(log_path, 'w', encoding=encoding, errors='ignore')
        f.close()
    return get_logger(name, level, write_to_file=True, log_path=log_path)


def log_need_to_be_updated(log_dir):
    import os
    current_dt = date_time.current_date()
    log_path = '%s/log_%s' % (log_dir, current_dt)
    if not os.path.exists(log_path):
        return True
    return False


def get_logger(name, level, write_to_file=False, log_path=None, encoding='gbk'):
    """
    return a logger that prints messages to file.
    """

    logger = logging.getLogger(name)
    logger.setLevel(level=level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    console_handler = logging.StreamHandler()
    console_handler.setLevel(level)
    console_handler.setFormatter(formatter)

    logger.addHandler(console_handler)

    if write_to_file:
        if log_path:
            file_handler = logging.FileHandler(log_path, encoding=encoding)
            file_handler.setLevel(level)
            file_handler.setFormatter(formatter)
            logger.addHandler(file_handler)
        else:
            raise Exception("the arg **log_path** should be given.")

    return logger


if __name__ == '__main__':

    # logger = get_logger("test", logging.DEBUG, write_to_file=True, log_path='log.txt')
    # logger.debug("%d people come in.", 23)
    logger = get_logger_by_date("test", logging.DEBUG, log_dir='../log')
    logger.info("test2")

