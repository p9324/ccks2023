# encoding=utf-8


class ProcessHelper(object):

    def __init__(self, logger, num, block):
        self.logger = logger
        self.num = num
        self.block = block
        self.count = 0

    def take_a_step(self):

        self.count += 1
        if self.count == self.num:
            self.logger.info("total {} items processed.".format(self.count))
        elif self.count % self.block == 0:
            self.logger.info("{} items processed, total {} items.".format(self.count, self.num))


class ProcessPrinter(object):

    def __init__(self, num, block):
        self.num = num
        self.block = block
        self.count = 0

    def take_a_step(self):

        self.count += 1
        if self.count == self.num:
            print("total {} items processed.".format(self.count))
        elif self.count % self.block == 0:
            print("{} items processed, total {} items.".format(self.count, self.num))
