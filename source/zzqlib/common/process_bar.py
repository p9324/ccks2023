# coding = utf-8
import sys
import math


def process_bar(current, total):
    """打印进度条"""
    percent = '{:.2%}'.format(float(current) / float(total))
    sys.stdout.write('\r')  # 这个回车相当于光标回到行首
    sys.stdout.write("[%-50s] %s" % ("=" * int(math.floor(current * 50 / total)), percent))
    sys.stdout.flush()


def process_bar_with_msg(current, total, msg):
    """打印进度条，并且包含消息**msg**"""
    percent = '{:.2%}'.format(float(current) / float(total))
    sys.stdout.write('\r')  # 这个回车相当于光标回到行首
    sys.stdout.write("[%-50s] %s (%s)" % ("=" * int(math.floor(current * 50 / total)), percent, msg))
    sys.stdout.flush()
