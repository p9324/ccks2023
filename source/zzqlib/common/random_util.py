# encoding=utf-8
import random


def is_selected(part):
    """
    used for random selection, the likelihood is part/10.
    """
    _num = float(random.randint(0, 10) / 10)
    _target = float(part) / float(10)
    return _num <= _target


def binary_select(target_list, upper_bound):

    selected_list = []
    unselected_list = target_list

    while len(selected_list) < upper_bound:
        cand_list = unselected_list
        unselected_list = []
        for ele in cand_list:
            if is_selected(5):
                selected_list.append(ele)
            else:
                unselected_list.append(ele)

            if len(selected_list) == upper_bound:
                return selected_list

    return selected_list
